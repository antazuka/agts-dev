var dgbox1_Title;
var SideBarSlideFx;
var BottomBarMainTabState;

var map;
var metersPerUnit = 111319.4908;
var inPerUnit = OpenLayers.INCHES_PER_UNIT.m * metersPerUnit;
OpenLayers.INCHES_PER_UNIT["dd"] = inPerUnit;
OpenLayers.INCHES_PER_UNIT["degrees"] = inPerUnit;
OpenLayers.DOTS_PER_INCH = 96;


bodyscroll = new Fx.Scroll(document.body, {
		wait: false,
		duration: 500,
		offset: {'x': 0, 'y': 0},
		transition: Fx.Transitions.Quart.easeOut 
	});	
document.title = 'AGTS - '+ dgbox1_Title;
InitialComponent();


function InitialComponent()
{
	$('bt-search-go').addEvent('click', function(e){	
		dg1__SwithcUIModf(0);
		dg1__Refresh(1);
	});
	$('bt-reset-go').addEvent('click', function(e){	
		$('inp-dgbox1-normal-search-q').set('value','');
		dg1__SwithcUIModf(0);
		dg1__Refresh(1);
	});
	
	// data tab fx slide
	SideBarSlideFx = new Fx.Slide('map-sidebar', {
		mode: 'horizontal',
		duration: 250,
		transition: Fx.Transitions.Cubic.easeOut,
		onComplete: function(){
			olcont_PanZoomBarUpdatePosition();
			if(SideBarMainTabState==0) {
				if(BottomBarMainTabState==1) $('map-bottombar-shadow').setStyle('left',null);	
				$('map-sidebar').set('class', null);	
			} else {
				if(BottomBarMainTabState==1) $('map-bottombar-shadow').setStyle('left','381px');	
				$('map-sidebar').set('class', 'vertshadow');	
			}
						
					
		}
		
	}).hide();

	var SideBarSlideContainer = $('map-sidebar').getParent();
	SideBarSlideContainer.setStyle('position',null);

	$('map-sidebar-slide-toggle-show').setStyle('opacity', '0.75');	
	$('map-sidebar-slide-toggle-show').addEvent('mouseover', function(e){ 
		$('map-sidebar-slide-toggle-show').setStyle('opacity', '0.9');	
	});
	$('map-sidebar-slide-toggle-show').addEvent('mouseout', function(e){ 
		$('map-sidebar-slide-toggle-show').setStyle('opacity', '0.75');	
	});
	
	$('map-sidebar-slide-toggle-show').addEvent('click', function(e){
		e = new Event(e);
		SideBarSlideFx.slideIn();
		e.stop();
		SideBarMainTabState = 1;
		// .. moved to SideBarSlideFx.onComplete
		// if(BottomBarMainTabState==1) $('map-bottombar-shadow').setStyle('left','381px'); 
		
	});		
	$('map-sidebar-slide-toggle-hide').addEvent('click', function(e){
		e = new Event(e);
		SideBarSlideFx.slideOut();
		e.stop();
		SideBarMainTabState = 0;
		if(BottomBarMainTabState==1) $('map-bottombar-shadow').setStyle('left',null);
		
	});
	
	// bottom bar
	$('map-bottombar-slide-toggle-show').setStyle('opacity', '0.75');	
	$('map-bottombar-slide-toggle-show').addEvent('mouseover', function(e){ 
		$('map-bottombar-slide-toggle-show').setStyle('opacity', '0.9');	
	});
	$('map-bottombar-slide-toggle-show').addEvent('mouseout', function(e){ 
		$('map-bottombar-slide-toggle-show').setStyle('opacity', '0.75');	
	});
	
	$('map-bottombar-slide-toggle-show').addEvent('click', function(e){
		e = new Event(e);
		e.stop();
		BottomBarSwitchState(1);
	});		
	$('map-bottombar-slide-toggle-hide').addEvent('click', function(e){
		e = new Event(e);
		e.stop();
		BottomBarSwitchState(0);
	});		
	
	InitialMaps();
	//$$(OpenLayers_Control_PanZoomBar_Id).setStyle('left', '30px');
}

var DgRowPopUp=null;
function evhandleDgRowMouseOver(elm,lon,lat,content){
	elm.setStyle('background-color','#FCFCFC');
}
function evhandleDgRowMouseOut(elm){
	elm.setStyle('background-color',null);
	if($defined(DgRowPopUp)){
		map.removePopup(DgRowPopUp);
		DgRowPopUp=null;
	}
}

function olcont_PanZoomBarUpdatePosition(){
	switch (SideBarMainTabState){
		case 0:
			$(OpenLayers_Control_PanZoomBar_Id).setStyle('top', '30px');	
			$$(OpenLayers_Control_PanZoomBar_Id).setStyle('left', '30px');
			$$(OpenLayers_Control_ScaleLine_Id).setStyle('left', '100px');						
			break;
		case 1:
			$$(OpenLayers_Control_PanZoomBar_Id).setStyle('top', '30px');	
			$$(OpenLayers_Control_PanZoomBar_Id).setStyle('left', '400px');
			$$(OpenLayers_Control_ScaleLine_Id).setStyle('left', '400px');
			break;
	}
}

function InitialMaps()
{
	if(map) map.destroy();

	renderer = OpenLayers.Util.getParameters(window.location.href).renderer;
	renderer = (renderer) ? [renderer] : OpenLayers.Layer.Vector.prototype.renderers;
		
	var bounds = new OpenLayers.Bounds();
	var proj = new OpenLayers.Projection("EPSG:4326");
	var proj_2 = new OpenLayers.Projection("EPSG:900913");
	bounds.extend(new OpenLayers.LonLat(95, -11).transform(proj, proj_2));
	bounds.extend(new OpenLayers.LonLat(150, 6).transform(proj, proj_2));
	
	var tempScales = [25900000,18000000,9000000,5000000,2000000,1000000,578000,289000,144000,72000,36000,18000,10000,5000];
	var LayerSwitcher = new OpenLayers.Control.LayerSwitcher({
						'roundedCorner': 'true', 
						'roundedCornerColor': '#ffffff'
	});
 
	var gsat = new OpenLayers.Layer.Google("Google Satellite", {
		type: google.maps.MapTypeId.SATELLITE,
		setIsBaseLayer : false,
		visibility: false,
		numZoomLevels: 15
	});
    var gphy = new OpenLayers.Layer.Google("Google Physical", {
		type: google.maps.MapTypeId.TERRAIN,
		visibility: true
	});
	var gmap = new OpenLayers.Layer.Google("Google Streets", {
		numZoomLevels: 15,
		setIsBaseLayer : true,
		visibility: true
	});
	var ghyb = new OpenLayers.Layer.Google("Google Hybrid", {
		type: google.maps.MapTypeId.HYBRID,
		numZoomLevels: 15,
		visibility: true
	});
	

	map = new OpenLayers.Map('map',{
		projection : proj_2,
		restrictedExtent: bounds,
			allowSelection: true,
			transitionEffect: null,
			zoomMethod: null,
			singleTile: true,
			controls : 
				[
					new OpenLayers.Control.Attribution(),
					//new OpenLayers.Control.Zoom(),
					new OpenLayers.Control.Navigation({ 
								dragPanOptions: { enableKinetic: true }
					}), // Tool Zoom
					new OpenLayers.Control.Scale(),
					new OpenLayers.Control.ScaleLine(),
					new OpenLayers.Control.PanZoomBar({zoomWorldIcon:'true',slideFactor:400}),
					LayerSwitcher,
					new OpenLayers.Control.MousePosition(),
					//new OpenLayers.Control.OverviewMap()
				],
			minZoomLevel: 5,
			scales : tempScales		
	});
	map.addLayers([gmap]);
	
	var lonLat = new OpenLayers.LonLat(106.841191, -6.182304).transform(proj, proj_2);
	map.render("map");
	map.zoomToExtent(bounds);
	//console.log(LayerSwitcher);
	//map.setCenter(lonLat,12);
	
	var featurecollection = {
					
					  "type": "Feature",
					  "geometry": {
						
						  "type": "Point",
						  "coordinates": [106.841191, -6.182304]
					  },
					  "properties": {  
						"id" : "123",
						"name": "Dinagat Islands",
						"picture" : "images/outlet.jpg"
					
					  }
    };
    var geojson_format = new OpenLayers.Format.GeoJSON(
			{
			internalProjection : proj_2,externalProjection : proj
			}
		
	);
	
	var colors = {
		//low: "rgb(181, 226, 140)", 
		//middle: "rgb(241, 211, 87)", 
		//high: "rgb(253, 156, 115)"
		
		low: "#1874CD", 
		middle: "#00CD66", 
		high: "#EE4000"
	};
	
	// Define three rules to style the cluster features.
	var lowRule = new OpenLayers.Rule({
		filter: new OpenLayers.Filter.Comparison({
			type: OpenLayers.Filter.Comparison.LESS_THAN,
			property: "count",
			value: 15
		}),
		symbolizer: {
			fillColor: colors.low,
			fillOpacity: 0.9, 
			strokeColor: colors.low,
			strokeOpacity: 0.5,
			strokeWidth: 10,
			pointRadius: 15,
			//label: "${count}",
			labelOutlineWidth: 1,
			fontColor: "#ffffff",
			fontOpacity: 0.8,
			fontSize: "12px",
			externalGraphic : "images/bluemarker.png"
		}
	});
	var middleRule = new OpenLayers.Rule({
		filter: new OpenLayers.Filter.Comparison({
			type: OpenLayers.Filter.Comparison.BETWEEN,
			property: "count",
			value: 1,
			lowerBoundary: 15,
			upperBoundary: 50
		}),
		symbolizer: {
			fillColor: colors.middle,
			fillOpacity: 0.9, 
			strokeColor: colors.middle,
			strokeOpacity: 0.5,
			strokeWidth: 12,
			pointRadius: 15,
			label: "${count}",
			labelOutlineWidth: 1,
			fontColor: "#ffffff",
			fontOpacity: 0.8,
			fontSize: "12px",
			externalGraphic : "images/orangemarker.png"
		}
	});
	var highRule = new OpenLayers.Rule({
		filter: new OpenLayers.Filter.Comparison({
			type: OpenLayers.Filter.Comparison.GREATER_THAN,
			property: "count",
			value: 1
		}),
		symbolizer: {
			fillColor: colors.high,
			fillOpacity: 0.9, 
			strokeColor: colors.high,
			strokeOpacity: 0.5,
			strokeWidth: 12,
			pointRadius: 20,
			label: "${count}",
			labelOutlineWidth: 1,
			fontColor: "#ffffff",
			fontOpacity: 0.8,
			fontSize: "12px",
			externalGraphic : "images/greenmarker.png"
		}
	});
	
	// Create a Style that uses the three previous rules
	var style = new OpenLayers.Style(null, {
		rules: [lowRule, middleRule, highRule]
	});   
	
	console.log(sourceDataWorld_cities);
    var vector_layer = new OpenLayers.Layer.Vector("Features",{
		protocol: new OpenLayers.Protocol.HTTP({
				url: sourceDataWorld_cities,
				format: geojson_format
		}),
		displayInLayerSwitcher : false,
		renderers: renderer,
		/*
		strategies: [
			new OpenLayers.Strategy.Fixed(),
			
			new OpenLayers.Strategy.AnimatedCluster({
				distance: 30,
				animationMethod: OpenLayers.Easing.Expo.easeOut,
				animationDuration: 20
			})
			],
			*/
		styleMap:  new OpenLayers.StyleMap(style)
	}); 
	vector_layer.addFeatures(geojson_format.read(featurecollection));
	vector_layer.addFeatures(features);
	map.addLayer(vector_layer);
	
	var features = [];
	for(var i=0; i< 50000; i++) {
		var lon = Math.random() * 2 + -4;
		var lat = Math.random() * 2 + 40;

		var lonlat = new OpenLayers.LonLat(lon, lat);
		lonlat.transform(new OpenLayers.Projection("EPSG:4326"), new OpenLayers.Projection("EPSG:900913"));

		var f = new OpenLayers.Feature.Vector( new OpenLayers.Geometry.Point(lonlat.lon, lonlat.lat));
		features.push(f);
	}  
	 
	function onPopupClose(evt) {
        selectVector.unselectAll();
		UnSelectVector();
    }
	function OnSelectVector(evt)
	{
		UnSelectVector();
		var evtFeature = evt.attributes.count;
		if(evtFeature == 1)
		{
			
			var popup = new OpenLayers.Popup.FramedCloud("contry", 
				evt.geometry.getBounds().getCenterLonLat(),
				new OpenLayers.Size(100,100),
				"<h2>"+evt.attributes.CNTRY_NAME + "</h2>" + evt.attributes.CITY_NAME,
				null, true, onPopupClose
			);
			evt.popup = popup;
			map.addPopup(popup);
		}
		
	
	}
	
	
	function UnSelectVector(evt)
	{
		var popupsIdx=map.popups.length-1;
			while(popupsIdx>-1){
				var popup = map.popups[popupsIdx];
				map.removePopup(popup);
				
				popupsIdx--;
			}
	}
    
	var selectVector = new OpenLayers.Control.SelectFeature(vector_layer,{
	
		multiple : false,
		clickout: true,
		multiple: true,
        toggle: true,
        autoActivate: true
		//onSelect : OnSelectVector,
		//onUnselect : UnSelectVector
		//hover : true
	});
	
	
	function onFeatureSelect(e) {
		
		UnSelectVector();
		var feature = e.feature;
		var evtFeature = feature.attributes.count;		
		if(evtFeature == 1)
		{
			//console.log(feature);
			var popup = new OpenLayers.Popup.FramedCloud("contry", 
				feature.geometry.getBounds().getCenterLonLat(),
				new OpenLayers.Size(100,100),
				"<h2>"+feature.cluster[0].attributes.CITY_NAME + "</h2>" + feature.cluster[0].attributes.CNTRY_NAME,
				null, true, onPopupClose
			);
			feature.popup = popup;
			map.addPopup(popup);
		}
		
		/*
		var description;
		var lonlat = map.getLonLatFromPixel(map.getControlsByClass("OpenLayers.Control.MousePosition")[0].lastXy);
		var feature = e.feature;
		if (this.name == "Vehicle") {
			description = 'Update: ' + feature.attributes.desc + '<br />' + 'Speed: ' + feature.attributes.speed + '<br />' +
				feature.attributes.iostatus;
		} else {
			description = feature.attributes.desc;
		}
		var popup = new OpenLayers.Popup.FramedCloud("featurePopup", lonlat, new OpenLayers.Size(100, 100), "<h2>" + feature.attributes.label + "</h2>" +
				description, null, true, onPopupClose);
		feature.popup = popup;
		map.addPopup(popup);
		*/
	}
	function onFeatureUnselect(e) {
		var feature = e.feature;
		if (feature.popup) {
			map.removePopup(feature.popup);
			feature.popup.destroy();
			feature.popup = null;
		}
	}
	
	map.addControl(selectVector);
	selectVector.activate();
	vector_layer.events.on({
			"featureselected" : onFeatureSelect,
			"featureunselected" : onFeatureUnselect
		});
	
	//Contoh MarkerCluster//
	
	
	//------event get lonLat from click map ----------------
	/*
	var CoordinateInfoMap_Popup='coordinateInfo';
	var OpenLayers_Popup=0;
	var MapControlState=1;
	map.events.register('click', map, function (e) {
		var exy = e.xy
		var thisPoint = map.getLonLatFromPixel(exy);
		//if(MapControlState==3){
			var popup = new OpenLayers.Popup.FramedCloud(
					CoordinateInfoMap_Popup+'_'+String(OpenLayers_Popup), 
					thisPoint,
					null,
					"Long: "+thisPoint.lon+"</br>Lat: "+thisPoint.lat,
					null,
					true
			);
			map.addPopup(popup,false);	
			OpenLayers_Popup++;
		//}
	});
	*/
	//--------------------------------------------------------------
	
}






	

