<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
Route::get('/regmodule', function(){
	$module = Module::all()->first();
	 echo $module->module_id;
	 echo $module->module_name;
	
});
*/

Route::get('/mapdashboard', 'MapDashboardController@index');
Route::get('/mapdashboard_getByPage', ['uses'=>'MapDashboardController@show', 'as' => 'mapdashboard_getByPage']);

