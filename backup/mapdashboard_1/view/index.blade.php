
@extends('layout.main_maps')

@section('content')
		
		
		<input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
		<div id="hde-messege-info" style="display:none;"> 				
		</div>  
		<div id="rgn-MODAL_CONTENT"></div>
		<div style="clear:both; width:100%; text-align:left; vertical-align:middle; padding:5px 5px 5px 5px; ">		
			<div style="clear:right; float:left; padding-right:15px;" class="ModuleTitle" >
				Map Dasboard
			</div>	
			<div id="top-pan-filter">
				<table style="font-size:12px;padding-top:8px;float:left">	
						<tr>
							
							<td>
								<input id="inp-dgbox1-normal-search-q" class="text-325 relative-left" maxLength="60"  type="text"></input>
							</td>
							<td>
								<a id="bt-search-go" class="button" style="font-size:10px;"><span>Search</span></a>
							</td>
							<td>
								<a id="bt-reset-go" class="button" style="font-size:10px;"><span>Reset</span></a>
							</td>
						</tr>
					</table>
					
					<div style="clear:right; float:right; padding-right:15px;" >
						
					</div>		
			</div>	
					
		</div>

		<hr class="style13">

		<div id="top-bottom-pan-filter" style="float:left; display:inline;">	
			<div  style="clear:left; float:left; padding:0px 0px 0px 10px;" >
			
				<table cellspacing="0" cellpadding="2" border="0" >
									
								  <tr>
									<td style="width:10px">&nbsp;</td>
									<td>
										<ul class="maptoolselector" style="float:left; ">
											<li class="active normal" id="maptool-pan"><img src="images/map-nav/mActionPan.png" class="maptoolimg"></li>
											<li class="inactive normal" id="maptool-zoomsel"><img src="images/map-nav/mActionZoomToSelected.png" class="maptoolimg"></li>
											<li class="inactive normal" id="maptool-coordinateinfo"><img src="images/map-nav/mActionCoordCapture.png" class="maptoolimg"></li>
											<li class="inactive normal" id="maptool-measure"><img src="images/map-nav/mActionMeasure.png" class="maptoolimg"><span id="maptool-measure-caption">&nbsp;&nbsp;Measure&nbsp;&nbsp;</span></li>
										</ul>
										<ul class="maptoolselector" style="float:left;margin-left:20px;">
											<li class="inactive normal" id="maptool-bookmark"><img src="images/icons/star1.png" class="maptoolimg">&nbsp;&nbsp;Bookmarks&nbsp;&nbsp;</li>
											<li class="inactive normal" id="maptool-findlocation" style="visibility:hidden;"><img src="images/map-nav/earth_find.png" class="maptoolimg">&nbsp;&nbsp;Find Location&nbsp;&nbsp;</li>						
										</ul>
										<ul class="maptoolselector" style="float:left;margin-left:20px;visibility:hidden;">
											<li class="inactive normal" id="maptool-print"><img src="images/icons/printmgr.png" class="maptoolimg">&nbsp;</li>
										</ul>
									</td>
								  </tr>   
				</table>
				
				
					
						
				
			</div>
			
		</div>	
	
		<div></div>
		<div id="map" style="top:125px;background-color:#99B3CC;"> 
		</div>
		<div id="map-sidebar" style="top:125px;">
			<div id="map-sidebar-header" style="background-color:white; top:125px; height:25px;">
				<div class="br-modf-head" >
					<ul id="inp-tabselector" class="tabselector" style="display:none;margin-left:-15px;">
						<li id="map-sidebar-header-tab0" class="item0 inactive" style="display:none;"></li>
						<li id="map-sidebar-header-tab1" class="item1 active" onclick="gTips_hide(this)" onmouseout="if(SideBarTabState!=1) gTips_hide(this)" onmouseover="if(SideBarTabState!=1) gTips_showb(this)" gtip="Customer Distribution"></li>
						<li id="map-sidebar-header-tab2" class="item2 inactive" onclick="gTips_hide(this)" onmouseout="if(SideBarTabState!=2) gTips_hide(this)" onmouseover="if(SideBarTabState!=2) gTips_showb(this)" gtip="Product Dashboard"></li>
						<li id="map-sidebar-header-tab3" class="item3 inactive" onclick="gTips_hide(this)" onmouseout="if(SideBarTabState!=3) gTips_hide(this)" onmouseover="if(SideBarTabState!=3) gTips_showb(this)" gtip="Salesman Dashboard"></li>
						<li id="map-sidebar-header-tab4" class="item4 inactive" onclick="gTips_hide(this)" onmouseout="if(SideBarTabState!=4) gTips_hide(this)" onmouseover="if(SideBarTabState!=4) gTips_showb(this)" gtip="Customer Dashboard"></li>
						<li id="map-sidebar-header-tab5" class="item5 inactive" onclick="gTips_hide(this)" onmouseout="if(SideBarTabState!=5) gTips_hide(this)" onmouseover="if(SideBarTabState!=5) gTips_showb(this)" gtip="Stock Supplies"></li>
						<li id="map-sidebar-header-tab9" class="item9 inactive"  ></li>
					</ul>
				</div>	
				<div id="map-sidebar-slide-toggle-hide" >
					<img id="slide-toggle-img-hide" src="images/icons/slide-side-hide.png" />
					<img id="slide-toggle-img-hiding" src="images/icons/slide-side-show.png" style="display:none;" />
				</div>
			</div>
			
			
			<!-- -CONTENT SECIOTN -->
			<div id="map-sidebar-content">		

				<div id="map-sidebar-tab0-container" style="clear: both; display: block; border-bottom:1px solid #E0E0E0; " class="br-content">
					<div id="map-sidebar-none-content-controls" style="padding:2px;">
						<span style="margin-top:0px;color: #DD9E00;font-size: 16px;" >Overview</span>	
						<div  style="margin-top:5px;margin-bottom:5px; margin-right:12px;">
							<div class="br-content">
								<!-- <img src="images/icons/sm-faq.gif" style="float:left;">	-->											
								<div style="font-size:11px;color:silver;">Pick up following task feature to explore data distribution.</div>													
							</div>
						</div>	
					</div>
					<!-- CONTENT -- BEGIN --->						
					<div id="map-sidebar-none-content-items" style="display:block; width:95%; position:absolute; top:70px; right:0px; bottom:20px; overflow:hidden;">
						<div>
							<div style="margin-right:32px;text-align:left;padding:6px;" class="dgsectionrow" onmouseout="evhandleDgRowMouseOut(this);" onmouseover="evhandleDgRowMouseOver(this)" >								
								
								<div style="margin-left:45px;margin-top:10px;font-weight:normal;font-size:14px;color:gray;font-family:Trebuchet, 'Trebuchet MS';">Displays customer location on map</div>
							</div>
							<div style="margin-right:32px;margin-top:10px;text-align:left;padding:6px;" class="dgsectionrow" onmouseout="evhandleDgRowMouseOut(this);" onmouseover="evhandleDgRowMouseOver(this)" >
								
								<div style="margin-left:45px;margin-top:10px;font-weight:normal;font-size:14px;color:gray;font-family:Trebuchet, 'Trebuchet MS';">Displays sales trend & quantity</div>
							</div>
							<div style="margin-right:32px;margin-top:10px;text-align:left;padding:6px;" class="dgsectionrow" onmouseout="evhandleDgRowMouseOut(this);" onmouseover="evhandleDgRowMouseOver(this)" >
								
								<div style="margin-left:45px;margin-top:10px;font-weight:normal;font-size:14px;color:gray;font-family:Trebuchet, 'Trebuchet MS';">Analyzes salesman's activity & performance</div>
							</div>
							<div style="margin-right:32px;margin-top:10px;text-align:left;padding:6px;" class="dgsectionrow" onmouseout="evhandleDgRowMouseOut(this);" onmouseover="evhandleDgRowMouseOver(this)" >
								
								<div style="margin-left:45px;margin-top:10px;font-weight:normal;font-size:14px;color:gray;font-family:Trebuchet, 'Trebuchet MS';">Calculates sales amount of customer</div>
							</div>
							<div style="margin-right:32px;margin-top:10px;text-align:left;padding:6px;" class="dgsectionrow" onmouseout="evhandleDgRowMouseOut(this);" onmouseover="evhandleDgRowMouseOver(this)" >
								
								<div style="margin-left:45px;margin-top:10px;font-weight:normal;font-size:14px;color:gray;font-family:Trebuchet, 'Trebuchet MS';">Displays customer stock</div>
							</div>
						</div>
					</div>									
					<!-- CONTENT -- END --->	
					<div id="map-sidebar-none-content-footer" style="width:100%;right:0px; position:absolute; bottom:0px; height:19px; border-top:1px solid #E0E0E0; ">
						<div style="width:auto; height:9px; font-family:Arial;font-size:10px;text-align: center; padding:5px 5px 5px 5px ;">
											
						</div>					
					</div>					
				</div>
				
				<div id="map-sidebar-tab1-container" style="height:55px;clear: both; display: none; border-bottom:1px solid #E0E0E0; " class="br-content">
					<div id="map-sidebar-tab1-content-controls" style="padding:6px;">
						<span style="margin-top:0px;color: #DD9E00;font-weight:bold;">Customer Distribution</span>
						<div style="margin-top:6px;">
							<input type="hidden" id="inp-DASHBOARD_CUSTOMER_PARAM1_VALUE">
							<div id="inp-rgn-DASHBOARD_CUSTOMER_PARAM1" style="width:100px;display:inline;">
								<input type="text" id="inp-DASHBOARD_CUSTOMER_PARAM1_TEXT"  class="text-225"  style="padding-right: 12px;width:90px;" readonly title="Region...">
								<img class="picker inElement" id="inp-DASHBOARD_CUSTOMER_PARAM1-picker" src="images/icons/picker.png" style="cursor: pointer; margin-bottom: 1px;" />	
							</div>
							<div id="inp-DASHBOARD_CUSTOMER_PARAM1_CONTAINER" class="dropeddown-container" style="display:none;width:347px;height:250px;">
								<iframe id="inp-DASHBOARD_CUSTOMER_PARAM1_ITEMS" src="" width="100%" height="100%" frameborder="0"></iframe>
							</div>	

							<select id="inp-CUST_FILTER" style="height:22px;width:120px;position: absolute;margin-left: 10px; margin-top: 1px;" title="Filter by...">
								<optgroup label="by visit...">
									<option value="6" selected>All visits criteria</option>
									<option value="1">1 month visits</option>
									<option value="2">1 month no visits</option>
									<option value="3">3 month no visits</option> 
									<option value="4">6 month no visits</option> 
									<option value="5">1 year no visits</option> 
									<option value="0">no any visits</option>	
								</optgroup>
								<optgroup label="by order...">
									<option value="16">All orders criteria</option>
									<option value="11">1 month orders</option>
									<option value="12">1 month no orders</option>
									<option value="13">3 month no orders</option> 
									<option value="14">6 month no orders</option> 
									<option value="15">1 year no orders</option> 
									<option value="10">no any orders</option>	
								</optgroup>
							</select>	
							<label id="inp-CUST_FILTER-label" class="overTxtLabel" style="line-height: normal; position: absolute; cursor: default; left: 5px; top: 5px; display:none;" for="inp-CUST_FILTER">Filter by...</label>		
							
							
						</div>
						<div style="float:right;position:absolute;top:30px; right:5px;width: 120px;">
							<input type="hidden" id="inp-DASHBOARD_CUSTOMER_PARAM2_VALUE">
							<div id="inp-rgn-DASHBOARD_CUSTOMER_PARAM2" style="width:100px;display:none;">
								<input type="text" id="inp-DASHBOARD_CUSTOMER_PARAM2_TEXT"  class="text-225"  style="padding-right: 12px;width:90px;" readonly title="Product...">
								<img class="picker inElement" id="inp-DASHBOARD_CUSTOMER_PARAM2-picker" src="images/icons/picker.png" style="cursor: pointer; margin-bottom: 1px;" />	
							</div>
							<div id="inp-DASHBOARD_CUSTOMER_PARAM2_CONTAINER" class="dropeddown-container" style="display:none;width:347px;height:250px;">
								<iframe id="inp-DASHBOARD_CUSTOMER_PARAM2_ITEMS" src="" width="100%" height="100%" frameborder="0"></iframe>
							</div>
						</div>
						<a style="font-size:11px; color:blue; cursor:pointer;display:none;" class="tabcontentLink" id="bt-tab1-refresh"><span>Refresh</span></a>
					</div>
					<!-- CONTENT -- BEGIN --->						
					<div id="map-sidebar-tab1-content-items" style="display:block; width:99%; position:absolute; top:61px; right:0px; bottom:50px; overflow:hidden; overflow-y:auto;">

					</div>									
					<!-- CONTENT -- END --->	
					<div id="map-sidebar-tab1-content-footer" style="width:100%;right:0px; position:absolute; bottom:0px; height:50px; border-top:1px solid #E0E0E0; ">
						<a class="button button-blue" style="font-size:9px; margin-left: 20px; margin-top: 10px;" id="bt-DASHBOARD_CUSTOMER_EXPORT"><span>Export</span></a>
						<div id="bt-DASHBOARD_CUSTOMER_EXPORT_CONTAINER" class="dropeddownexp-container" style="display:none;margin-top:1px; height: auto; width:130px;">
							<div style="color:white;background-color:#3779BA;margin:0;display:none;" class="exportItemContainer blue"> 
								<div onclick="DashboardCustomer_Export_1(this);" onmouseout="javascript:evhandleExportItem_mouseout(this);" onmouseover="javascript:evhandleExportItem_mouseover(this);" class="exportItem">Customer Disribution Summary</div> 
							</div>													
							<div style="color:white;background-color:#3779BA;margin:0;" class="exportItemContainer blue"> 
								<div onclick="DashboardCustomer_Export_2(this);" onmouseout="javascript:evhandleExportItem_mouseout(this);" onmouseover="javascript:evhandleExportItem_mouseover(this);" class="exportItem">Customer Disribution Detail</div> 
							</div>												
						</div>						
						<div style="width:auto; height:20px; font-family:Arial; font-size:10px; text-align: right; right:10px;top:8px; position:absolute;">							
							<div id="lbl-tab1-itemscount" style="font-size: 12px;margin-bottom: 5px; margin-right: 5px; ">-</div>
							<a id="bt-tab1-nav-first-1" class="tabcontentLink" style="font-size:12px; color:blue; cursor:pointer;display:none;">&laquo; First</a>&nbsp;
							<a id="bt-tab1-nav-prev-1" class="tabcontentLink" style="font-size:12px; color:blue; cursor:pointer;display:none;">&lsaquo; Previous</a>&nbsp;
							<span id="bt-tab1-nav-current-1" style="font-size:12px; display:inline;"></span>&nbsp;
							<a id="bt-tab1-nav-next-1" class="tabcontentLink" style="font-size:12px; color:blue; cursor:pointer;display:none;">Next &rsaquo;</a>&nbsp;
							<a id="bt-tab1-nav-last-1" class="tabcontentLink" style="font-size:12px; color:blue; cursor:pointer;display:none;">Last &raquo;</a>&nbsp;  

							<div style="display:none;">	
								<a id="bt-tab1-nav-first-2" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">&laquo; First</a>&nbsp;
								<a id="bt-tab1-nav-prev-2" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">&lsaquo; Previous</a>&nbsp;
								<span id="bt-tab1-nav-current-2" style="font-size:11px; display:inline;"></span>&nbsp;
								<a id="bt-tab1-nav-next-2" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">Next &rsaquo;</a>&nbsp;
								<a id="bt-tab1-nav-last-2" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">Last &raquo;</a>&nbsp;                            	
							</div>					
						</div>					
					</div>					
				</div>	
				
				<!-- product -->
				<div id="map-sidebar-tab2-container" style="clear: both; display: none; border-bottom:1px solid #E0E0E0;background-color:#fff; height: 50px;" class="br-content">
					<div id="map-sidebar-tab2-content-controls" style="padding:3px;">

						<div id="rgn-dashboarditem1-description" style="visibility:hidden;color:silver;font-size:11px;margin-top:2px;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </div>
					</div>
					<!-- CONTENT -- BEGIN --->						
					<div id="map-sidebar-tab2-content-items" style="width:100%; position:absolute; top:58px; right:0px; bottom:0px; overflow-x:hidden; overflow-y:auto;">
							<div style="background-color:#fff;border-color:#eaeaea;left:42px;top:0px; right:0px; bottom:0px; border-style:solid;border-width:1px 0px 0px 1px;padding:0;position:absolute; margin: 0;">										
								<div id="rgn-dashboard-item11" style="padding:10px;display:none;">								
									<table cellspacing="0" cellpadding="4" border="0" width="99%" style="margin:10px 0px 0px 10px">
										<tr>
											<td colspan="2">
												<input type="hidden" id="inp-DASHBOARD_ITEM11_PARAM1_VALUE">
												<label for="inp-DASHBOARD_ITEM11_PARAM1_TEXT" >	
													<div><b>Product :</b></div>
													<div id="inp-rgn-DASHBOARD_ITEM11_PARAM1" style="width:300px;display:inline;">
														<input type="text" id="inp-DASHBOARD_ITEM11_PARAM1_TEXT"  class="text-225"  style="padding-right: 22px;width:265px;" readonly>
														<img class="picker inElement" id="inp-DASHBOARD_ITEM11_PARAM1-picker" src="images/icons/picker.png" style="cursor: pointer; margin-bottom: 1px;" />	
													</div>
													<div id="inp-DASHBOARD_ITEM11_PARAM1_CONTAINER" class="dropeddown-container" style="display:none; height: 250px;">
														<iframe id="inp-DASHBOARD_ITEM11_PARAM1_ITEMS" src="" width="100%" height="100%" frameborder="0"></iframe>
													</div>
												</label>
											</td>
										</tr>
										<tr valign="bottom">
											<td align="left">
												<label for="inp-DASHBOARD_ITEM11_PARAM2" >	
													<div><b>Period :</b></div>								
													<select id="inp-DASHBOARD_ITEM11_PARAM2">
														<option value="0"></option>
														<option value="3">Recent 3 Months</option>
														<option value="6">Recent 6 Months</option> 
														<option value="12">Recent 12 Months</option> 
													</select>
												</label>											
											</td>
											<td align="right" style="padding-right:20px;">
												<a id="bt-DASHBOARD_ITEM11_EXPORT" style="font-size:9px;" class="button button-blue"><span>Export</span></a>
												<a id="bt-DASHBOARD_ITEM11_PROCESS" class="button button-blue" style="font-size:9px;"><span>Process</span></a>
												<div id="bt-DASHBOARD_ITEM11_EXPORT_CONTAINER" class="dropeddownexp-container" style="display:none;margin-top:1px; height: auto; width:130px;">
													<div style="color:white;background-color:#3779BA;margin:0;" class="exportItemContainer blue"> 
														<div onclick="DashboardItem11_Export_1(this);" onmouseout="javascript:evhandleExportItem_mouseout(this);" onmouseover="javascript:evhandleExportItem_mouseover(this);" class="exportItem">Trend Product Summary</div> 
													</div>													
													<div style="color:white;background-color:#3779BA;margin:0;" class="exportItemContainer blue"> 
														<div onclick="DashboardItem11_Export_2(this);" onmouseout="javascript:evhandleExportItem_mouseout(this);" onmouseover="javascript:evhandleExportItem_mouseover(this);" class="exportItem">Trend Product Detail</div> 
													</div>												
												</div>
											</td>
										</tr>
									</table>																	
								</div>					
								<div id="rgn-dashboard-item12" style="padding:10px;display:none;">										
									<table cellspacing="0" cellpadding="4" border="0" width="99%" style="margin:10px 0px 0px 10px">	
										<tr>
											<td colspan="2">
												<input type="hidden" id="inp-DASHBOARD_ITEM12_PARAM3_VALUE">
												<label for="inp-DASHBOARD_ITEM12_PARAM3_TEXT" >	
													<div><b>Region :</b></div>
													<div id="inp-rgn-DASHBOARD_ITEM12_PARAM3" style="width:300px;display:inline;">
														<input type="text" id="inp-DASHBOARD_ITEM12_PARAM3_TEXT"  class="text-225"  style="padding-right: 22px;width:265px;" readonly>
														<img class="picker inElement" id="inp-DASHBOARD_ITEM12_PARAM3-picker" src="images/icons/picker.png" style="cursor: pointer; margin-bottom: 1px;" />	
													</div>
													<div id="inp-DASHBOARD_ITEM12_PARAM3_CONTAINER" class="dropeddown-container" style="display:none;">
														<iframe id="inp-DASHBOARD_ITEM12_PARAM3_ITEMS" src="" width="100%" height="100%" frameborder="0"></iframe>
													</div>
												</label>
											</td>
										</tr>
										<tr>
											<td align="left" >
												<label for="inp-DASHBOARD_ITEM12_PARAM4" >	
													<div><b>Customer Type :</b></div>
													<div id="inp-rgn-DASHBOARD_ITEM12_PARAM4" style="display:inline;">
													</div>
												</label>
											</td>
											<td align="center">
											</td>
										</tr>								
										<tr valign="bottom">
											<td align="left">
												<table ellspacing="0" cellpadding="0" border="0" align="left">
													<tr>
														<td style="width:95px;" >
															<label for="inp-DASHBOARD_ITEM12_PARAM1">
																<div><b>From :</b></div>
																<div style="width:130px;display:inline;">
																	<input name='date_toggled' type="text" id="inp-DASHBOARD_ITEM12_PARAM1"  class="text-65"  style="padding-right: 22px;" onclick="javascript:ce1.show();" onKeyPress='return InputSlashNumbersOnly(this, event)'>
																	<img class="picker inElement" id="inp-DASHBOARD_ITEM12_PARAM1-picker" src="images/icons/calendar.gif" style="cursor: pointer; margin-bottom: 1px;" />														
																</div>
															</label>
														</td>
														<td style="width:10px;">
														</td>
														<td style="width:95px;">
															<label for="inp-DASHBOARD_ITEM12_PARAM2">
																<div><b>Until :</b></div>
																<div style="width:130px;display:inline;">
																	<input name='date_toggled' type="text" id="inp-DASHBOARD_ITEM12_PARAM2"  class="text-65"  style="padding-right: 22px;" onclick="javascript:ce2.show();" onKeyPress='return InputSlashNumbersOnly(this, event)'>
																	<img class="picker inElement" id="inp-DASHBOARD_ITEM12_PARAM2-picker" src="images/icons/calendar.gif" style="cursor: pointer; margin-bottom: 1px;" />														
																</div>
															</label>
														</td>
													</tr>
												</table>										
											</td>
											<td align="center">
											</td>
										</tr>
										<tr valign="bottom">
											<td colspan="2" align="right" style="padding-right:20px;">
												<a id="bt-DASHBOARD_ITEM12_EXPORT" style="font-size:9px;" class="button button-blue"><span>Export</span></a>
												<a id="bt-DASHBOARD_ITEM12_PROCESS" class="button button-blue" style="font-size:9px;"><span>Process</span></a>
												<div id="bt-DASHBOARD_ITEM12_EXPORT_CONTAINER" class="dropeddownexp-container" style="display:none;margin-top:1px; height: auto;">
													<div style="color:white;background-color:#3779BA;margin:0;" class="exportItemContainer blue"> 
														<div onclick="DashboardItem12_Export_1(this);" onmouseout="javascript:evhandleExportItem_mouseout(this);" onmouseover="javascript:evhandleExportItem_mouseover(this);" class="exportItem">Order Quantity Summary</div> 
													</div>			
													<div style="color:white;background-color:#3779BA;margin:0;" class="exportItemContainer blue"> 
														<div onclick="DashboardItem12_Export_2(this);" onmouseout="javascript:evhandleExportItem_mouseout(this);" onmouseover="javascript:evhandleExportItem_mouseover(this);" class="exportItem">Order Quantity Detail</div> 
													</div>												
												</div>
											</td>
										</tr>
									</table>	
								</div>	
								<div id="rgn-dashboard-item13" style="padding:10px;display:none;">
									<table cellspacing="0" cellpadding="4" border="0" width="99%" style="margin:10px 0px 0px 10px">
										<tr>
											<td colspan="2">
												<input type="hidden" id="inp-DASHBOARD_ITEM13_PARAM1_VALUE">
												<label for="inp-DASHBOARD_ITEM13_PARAM1_TEXT" >	
													<div><b>Product :</b></div>
													<div id="inp-rgn-DASHBOARD_ITEM13_PARAM1" style="width:300px;display:inline;">
														<input type="text" id="inp-DASHBOARD_ITEM13_PARAM1_TEXT"  class="text-225"  style="padding-right: 22px;width:265px;" readonly>
														<img class="picker inElement" id="inp-DASHBOARD_ITEM13_PARAM1-picker" src="images/icons/picker.png" style="cursor: pointer; margin-bottom: 1px;" />	
													</div>
													<div id="inp-DASHBOARD_ITEM13_PARAM1_CONTAINER" class="dropeddown-container" style="display:none;">
														<iframe id="inp-DASHBOARD_ITEM13_PARAM1_ITEMS" src="" width="100%" height="100%" frameborder="0"></iframe>
													</div>
												</label>
											</td>
										</tr>
										<tr valign="bottom">
											<td align="left">
												<label for="inp-DASHBOARD_ITEM13_PARAM2" >	
													<div><b>Period :</b></div>								
													<select id="inp-DASHBOARD_ITEM13_PARAM2">
														<option value="0"></option>
														<option value="3">Recent 3 Months</option>
														<option value="6">Recent 6 Months</option> 
														<option value="12">Recent 12 Months</option> 
													</select>
												</label>											
											</td>
											<td align="right" style="padding-right:20px;">
												<a id="bt-DASHBOARD_ITEM13_EXPORT" style="font-size:9px;" class="button button-blue"><span>Export</span></a>
												<a id="bt-DASHBOARD_ITEM13_PROCESS" class="button button-blue" style="font-size:9px;"><span>Process</span></a>
												<div id="bt-DASHBOARD_ITEM13_EXPORT_CONTAINER" class="dropeddownexp-container" style="display:none;margin-top:1px; height: auto;">									
													<div style="color:white;background-color:#3779BA;margin:0;" class="exportItemContainer blue"> 
														<div onclick="DashboardItem13_Export_1(this);" onmouseout="javascript:evhandleExportItem_mouseout(this);" onmouseover="javascript:evhandleExportItem_mouseover(this);" class="exportItem">Trend Product Summary</div> 
													</div>													
													<div style="color:white;background-color:#3779BA;margin:0;" class="exportItemContainer blue"> 
														<div onclick="DashboardItem13_Export_2(this);" onmouseout="javascript:evhandleExportItem_mouseout(this);" onmouseover="javascript:evhandleExportItem_mouseover(this);" class="exportItem">Trend Product Detail</div> 
													</div>														
												</div>
											</td>
										</tr>
									</table>		
								</div>									
								<div id="rgn-dashboard-item14" style="padding:10px;display:none;">
									<table cellspacing="0" cellpadding="4" border="0" width="99%" style="margin:10px 0px 0px 10px">	
										<tr>
											<td colspan="2">
												<input type="hidden" id="inp-DASHBOARD_ITEM14_PARAM1_VALUE">
												<label for="inp-DASHBOARD_ITEM14_PARAM1_TEXT" >	
													<div><b>Region :</b></div>
													<div id="inp-rgn-DASHBOARD_ITEM14_PARAM1" style="width:300px;display:inline;">
														<input type="text" id="inp-DASHBOARD_ITEM14_PARAM1_TEXT"  class="text-225"  style="padding-right: 22px;width:265px;" readonly>
														<img class="picker inElement" id="inp-DASHBOARD_ITEM14_PARAM1-picker" src="images/icons/picker.png" style="cursor: pointer; margin-bottom: 1px;" />	
													</div>
													<div id="inp-DASHBOARD_ITEM14_PARAM1_CONTAINER" class="dropeddown-container" style="display:none;">
														<iframe id="inp-DASHBOARD_ITEM14_PARAM1_ITEMS" src="" width="100%" height="100%" frameborder="0"></iframe>
													</div>
												</label>
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<input type="hidden" id="inp-DASHBOARD_ITEM14_PARAM2_VALUE">
												<label for="inp-DASHBOARD_ITEM14_PARAM2_TEXT" >	
													<div><b>Product :</b></div>
													<div id="inp-rgn-DASHBOARD_ITEM14_PARAM2" style="width:300px;display:inline;">
														<input type="text" id="inp-DASHBOARD_ITEM14_PARAM2_TEXT"  class="text-225"  style="padding-right: 22px;width:265px;" readonly>
														<img class="picker inElement" id="inp-DASHBOARD_ITEM14_PARAM2-picker" src="images/icons/picker.png" style="cursor: pointer; margin-bottom: 1px;" />	
													</div>
													<div id="inp-DASHBOARD_ITEM14_PARAM2_CONTAINER" class="dropeddown-container" style="display:none; height:250px;">
														<iframe id="inp-DASHBOARD_ITEM14_PARAM2_ITEMS" src="" width="100%" height="100%" frameborder="0"></iframe>
													</div>
												</label>
											</td>
										</tr>
										<tr>
											<td>
												<label for="inp-DASHBOARD_ITEM14_PARAM3" >	
													<div><b>Average Period :</b></div>
													<select id="inp-DASHBOARD_ITEM14_PARAM3" style="width:85px">
														<option value="0" selected="selected"></option>
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
														<option value="5">5</option>
														<option value="6">6</option>
														<!--
														<option value="7">7</option>
														<option value="8">8</option>
														<option value="9">9</option>
														<option value="10">10</option>
														<option value="11">11</option>
														<option value="12">12</option>
														-->
													</select>	
												</label>
											</td>
											<td>
												<label for="inp-DASHBOARD_ITEM14_PARAM4" >	
													<div><b>Forecast Period :</b></div>
													<select id="inp-DASHBOARD_ITEM14_PARAM4" style="width:85px">
														<option value="0" selected="selected"></option>
														<option value="1">1</option>
														<option value="2">2</option>
														<option value="3">3</option>
														<option value="4">4</option>
														<option value="5">5</option>
														<option value="6">6</option>
														<!--
														<option value="7">7</option>
														<option value="8">8</option>
														<option value="9">9</option>
														<option value="10">10</option>
														<option value="11">11</option>
														<option value="12">12</option>
														-->
													</select>	
												</label>
											</td>
										</tr>			
										<tr valign="bottom">
											<td colspan="2" align="right" style="padding-right:20px;">
												<a id="bt-DASHBOARD_ITEM14_EXPORT" style="font-size:9px;" class="button button-blue"><span>Export</span></a>
												<a id="bt-DASHBOARD_ITEM14_PROCESS" class="button button-blue" style="font-size:9px;"><span>Process</span></a>
												<div id="bt-DASHBOARD_ITEM14_EXPORT_CONTAINER" class="dropeddownexp-container" style="margin-top:1px; height: auto;">
													<div style="color:white;background-color:#3779BA;margin:0;" class="exportItemContainer blue"> 
														<div onclick="DashboardItem14_Export_1(this);" onmouseout="javascript:evhandleExportItem_mouseout(this);" onmouseover="javascript:evhandleExportItem_mouseover(this);" class="exportItem">Order Forecast Detail</div> 
													</div>			
													<div style="color:white;background-color:#3779BA;margin:0;display:none;" class="exportItemContainer blue"> 
														<div onclick="DashboardItem14_Export_2(this);" onmouseout="javascript:evhandleExportItem_mouseout(this);" onmouseover="javascript:evhandleExportItem_mouseover(this);" class="exportItem">Order Forecast Detail</div> 
													</div>												
												</div>
											</td>
										</tr>
									</table>											
								</div>					
								<div id="rgn-dashboard-item15" style="padding:10px;display:none;">
											
								</div>	
							</div>
							<ul id="dashboard-switcher" class="dashboardselector" style="position:absolute; width:40px;padding:0px 0px 0px 3px;margin: 8px 0px 0px 0px;">
								<li id="dashboard-switcher-tab11" class="item11 inactive" onclick="gTips_hide(this)" onmouseout="if(DasboardItem1TabState!=1) gTips_hide(this)" onmouseover="if(DasboardItem1TabState!=1) gTips_showd(this)" gtip="Trend Quantity of Taking-Order" desc="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."></li>
								<li id="dashboard-switcher-tab13" class="item13 inactive" onclick="gTips_hide(this)" onmouseout="if(DasboardItem1TabState!=3) gTips_hide(this)" onmouseover="if(DasboardItem1TabState!=3) gTips_showd(this)" gtip="Trend Amount of Taking-Order" desc="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."></li>
								<li id="dashboard-switcher-tab12" class="item12 inactive" onclick="gTips_hide(this)" onmouseout="if(DasboardItem1TabState!=2) gTips_hide(this)" onmouseover="if(DasboardItem1TabState!=2) gTips_showd(this)" gtip="Order Quantity" desc="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."></li>
								<li id="dashboard-switcher-tab14" class="item14 inactive" onclick="gTips_hide(this)" onmouseout="if(DasboardItem1TabState!=4) gTips_hide(this)" onmouseover="if(DasboardItem1TabState!=4) gTips_showd(this)" gtip="Order Forecast" desc="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."></li>
								<li id="dashboard-switcher-tab15" class="item15 inactive" onclick="gTips_hide(this)" onmouseout="if(DasboardItem1TabState!=4) gTips_hide(this)" onmouseover="if(DasboardItem1TabState!=4) gTips_showd(this)" gtip="Reserve" style="visibility:hidden;" desc="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."></li>
							</ul>
					</div>										
					<!-- CONTENT -- END --->	
					<div id="map-sidebar-tab2-content-footer" style="background-color:#fff;width:100%;right:0px; position:absolute; bottom:0px; height:19px; border-top:1px solid #E0E0E0; display:none; ">
						<div style="width:auto; height:20px; font-family:Arial;font-size:10px;text-align: center; padding:5px 5px 5px 5px ;display:none;">
							<a id="bt-tab2-nav-first-1" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">&laquo; First</a>&nbsp;
							<a id="bt-tab2-nav-prev-1" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">&lsaquo; Previous</a>&nbsp;
							<span id="bt-tab2-nav-current-1" style="font-size:11px; display:inline;"></span>&nbsp;
							<a id="bt-tab2-nav-next-1" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">Next &rsaquo;</a>&nbsp;
							<a id="bt-tab2-nav-last-1" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">Last &raquo;</a>&nbsp;  

							<div style="display:none;">	
								<a id="bt-tab2-nav-first-2" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">&laquo; First</a>&nbsp;
								<a id="bt-tab2-nav-prev-2" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">&lsaquo; Previous</a>&nbsp;
								<span id="bt-tab2-nav-current-2" style="font-size:11px; display:inline;"></span>&nbsp;
								<a id="bt-tab2-nav-next-2" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">Next &rsaquo;</a>&nbsp;
								<a id="bt-tab2-nav-last-2" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">Last &raquo;</a>&nbsp;                            	
							</div>					
						</div>					
					</div>					
				</div>	

				<!-- salesman -->
				<div id="map-sidebar-tab3-container" style="clear: both; display: none; border-bottom:1px solid #E0E0E0;background-color:#fff; height: 50px;" class="br-content">
					<div id="map-sidebar-tab3-content-controls" style="padding:3px;">						
						<span style="margin-top:0px;color: #DD9E00;font-weight:bold;">Salesman Dashboard</span>
					
						<span style="font-size:12px;color:blue;" id="rgn-dashboarditem2-subtitle">-</span>
						<div id="rgn-dashboarditem2-description" style="visibility:hidden;color:silver;font-size:11px;margin-top:2px;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </div>
					</div>
					<!-- CONTENT -- BEGIN --->						
					<div id="map-sidebar-tab3-content-items" style="width:100%; position:absolute; top:58px; right:0px; bottom:0px; overflow-x:hidden; overflow-y:auto;">
							<div style="background-color:#fff;border-color:#eaeaea;left:42px;top:0px; right:0px; bottom:0px; border-style:solid;border-width:1px 0px 0px 1px;padding:0;position:absolute; margin: 0;">							
								<div id="rgn-dashboard-item21" style="padding:10px;display:none;">
										
									<table cellspacing="0" cellpadding="4" border="0" width="99%" style="margin:10px 0px 0px 10px">
											<tr>
												<td  style="width:auto;" colspan="2">
													<label for="inp-DASHBOARD_ITEM21_PARAM1" >	
														<div><b>Salesman :</b></div>
														<div id="inp-rgn-DASHBOARD_ITEM21_PARAM1"></div>
													</label>
												</td>
											</tr>
											<tr valign="bottom">
												<td style="width:110px;" >
														<label for="inp-DASHBOARD_ITEM21_PARAM2">
															<div><b>Date :</b></div>
															<div style="width:130px;display:inline;">
																<input name='date_toggled' type="text" id="inp-DASHBOARD_ITEM21_PARAM2"  class="text-65"  style="padding-right: 22px;" onclick="javascript:ce9.show();" onKeyPress='return InputSlashNumbersOnly(this, event)'>
																<img class="picker inElement" id="inp-DASHBOARD_ITEM21_PARAM2-picker" src="images/icons/calendar.gif" style="cursor: pointer; margin-bottom: 1px;" />														
															</div>
														</label>
												</td>
												<td align="right" style="padding-right:20px;">
													<a id="bt-DASHBOARD_ITEM21_EXPORT" style="font-size:9px;" class="button button-blue"><span>Export</span></a>
													<a id="bt-DASHBOARD_ITEM21_PROCESS" class="button button-blue" style="font-size:9px;"><span>Process</span></a>
													<div id="bt-DASHBOARD_ITEM21_EXPORT_CONTAINER" class="dropeddownexp-container" style="display:none;margin-top:1px; height: auto;">
														<div style="color:white;background-color:#3779BA;margin:0;" class="exportItemContainer blue"> 
															<div onclick="DashboardItem21_Export_1(this);" onmouseout="javascript:evhandleExportItem_mouseout(this);" onmouseover="javascript:evhandleExportItem_mouseover(this);" class="exportItem">Taking Order</div> 
														</div>												
													</div>
												</td>
											</tr>
									</table>	
									
									<ul class="subtabselector" id="map-sidebar-tab3-content-items-subtab" style="display:none;">
										<li id="dashboard-switcher-subtab211" class="active">Visit List</li>
										<li id="dashboard-switcher-subtab212" class="inactive">Duration Visit</li>
										<li id="dashboard-switcher-subtab213" class="inactive">Interval Visit</li>
									</ul>
									
									<div id="rgn-sidebar-tab31-content-items-listdata" style="border-top:2px dotted #F2F2F2; display:none; background-color:#ff;left:0px; top:145px; right:0px; bottom:20px; padding:0;position:absolute; margin: 0;overflow-x:hidden; overflow-y:auto;">		
									</div>	
									<div id="rgn-sidebar-tab31-content-items-durationvisit" style="border-top:2px dotted #F2F2F2; display:none; background-color:#ff;left:0px; top:145px; right:0px; bottom:20px; padding:0;position:absolute; margin: 0;overflow-x:hidden; overflow-y:auto;">		
											<div style="padding:6px;">
												<div>
													<span style="color: #055A78;font-weight:bold">Form</span>&nbsp;<span id="rgn-ORD_START_DATE1">-</span>&nbsp;&nbsp;<span style="color: #055A78;font-weight:bold">to</span>&nbsp;<span id="rgn-ORD_END_DATE1">-</span>
												</div>											
											</div>	
											<div style="padding:6px;">
												<table cellspacing="0" cellpadding="1" border="0" width="99%" style="margin:10px 0px 0px 10px">
														<tr>
															<td style="width:110px;font-weight:bold;">Visit</td>
															<td style="width:5px;">&nbsp;:&nbsp;</td>
															<td id="rgn-CVisit_Count1">-</td>
														</tr>
														<tr>
															<td style="width:110px;font-weight:bold;">Total Duration</td>
															<td style="width:5px;">&nbsp;:&nbsp;</td>
															<td id="rgn-CVisit_Duration_Total">-</td>
														</tr>
														<tr>
															<td style="width:110px;font-weight:bold;">Average Duration</td>
															<td style="width:5px;">&nbsp;:&nbsp;</td>
															<td id="rgn-CVisit_Duration_Avg">-</td>
														</tr>
												</table>	
											<div style="padding:6px;">	
											</div>	
												<table cellspacing="0" cellpadding="1" border="0" width="99%" style="margin:10px 0px 0px 10px">
														<tr>
															<td style="width:auto;font-weight:bold;color: #E09F06;" colspan="3">Maximum</td>
														</tr>
														<tr>
															<td style="width:65px;font-weight:bold;padding-left:20px;">Customer</td>
															<td style="width:5px;">&nbsp;:&nbsp;</td>
															<td id="rgn-CVisit_Duration_Max_Customer">-</td>
														</tr>
														<tr>
															<td style="width:65px;font-weight:bold;padding-left:20px;">Duration</td>
															<td style="width:5px;">&nbsp;:&nbsp;</td>
															<td id="rgn-CVisit_Duration_Max">-</td>
														</tr>
														<tr>
															<td style="width:auto;font-weight:bold;color: #E09F06;" colspan="3">Minimum</td>
														</tr>
														<tr>
															<td style="width:65px;font-weight:bold;padding-left:20px;">Customer</td>
															<td style="width:5px;">&nbsp;:&nbsp;</td>
															<td id="rgn-CVisit_Duration_Min_Customer">-</td>
														</tr>
														<tr>
															<td style="width:65px;font-weight:bold;padding-left:20px;">Duration</td>
															<td style="width:5px;">&nbsp;:&nbsp;</td>
															<td id="rgn-CVisit_Duration_Min">-</td>
														</tr>
												</table>	
											</div>		

									</div>	
									<div id="rgn-sidebar-tab31-content-items-intervalvisit" style="border-top:2px dotted #F2F2F2; display:none; background-color:#ff;left:0px; top:145px; right:0px; bottom:20px; padding:0;position:absolute; margin: 0;overflow-x:hidden; overflow-y:auto;">		
											<div style="padding:6px;">
												<div>
													<span style="color: #055A78;font-weight:bold">Form</span>&nbsp;<span id="rgn-ORD_START_DATE2">-</span>&nbsp;&nbsp;<span style="color: #055A78;font-weight:bold">to</span>&nbsp;<span id="rgn-ORD_END_DATE2">-</span>
												</div>											
											</div>	
											<div style="padding:6px;">
												<table cellspacing="0" cellpadding="1" border="0" width="99%" style="margin:10px 0px 0px 10px">
														<tr>
															<td style="width:110px;font-weight:bold;">Visit</td>
															<td style="width:5px;">&nbsp;:&nbsp;</td>
															<td id="rgn-CVisit_Count2">-</td>
														</tr>
														<tr>
															<td style="width:110px;font-weight:bold;">Total Duration</td>
															<td style="width:5px;">&nbsp;:&nbsp;</td>
															<td id="rgn-IVisit_Duration_Total">-</td>
														</tr>
														<tr>
															<td style="width:110px;font-weight:bold;">Average Duration</td>
															<td style="width:5px;">&nbsp;:&nbsp;</td>
															<td id="rgn-IVisit_Duration_Avg">-</td>
														</tr>
												</table>	
											<div style="padding:6px;">	
											</div>	
												<table cellspacing="0" cellpadding="1" border="0" width="99%" style="margin:10px 0px 0px 10px">
														<tr>
															<td style="width:auto;font-weight:bold;color: #E09F06;" colspan="3">Maximum</td>
														</tr>
														<tr>
															<td style="width:65px;font-weight:bold;padding-left:20px;">Duration</td>
															<td style="width:5px;">&nbsp;:&nbsp;</td>
															<td id="rgn-IVisit_Duration_Max">-</td>
														</tr>
														<tr>
															<td style="width:65px;font-weight:bold;padding-left:20px;">From</td>
															<td style="width:5px;">&nbsp;:&nbsp;</td>
															<td id="rgn-IVisit_Duration_Max_Customer_From">-</td>
														</tr>
														<tr>
															<td style="width:65px;font-weight:bold;padding-left:20px;">To</td>
															<td style="width:5px;">&nbsp;:&nbsp;</td>
															<td id="rgn-IVisit_Duration_Max_Customer_To">-</td>
														</tr>
														<tr>
															<td style="width:auto;font-weight:bold;color: #E09F06;" colspan="3">Minimum</td>
														</tr>
														<tr>
															<td style="width:65px;font-weight:bold;padding-left:20px;">Duration</td>
															<td style="width:5px;">&nbsp;:&nbsp;</td>
															<td id="rgn-IVisit_Duration_Min">-</td>
														</tr>
														<tr>
															<td style="width:65px;font-weight:bold;padding-left:20px;">From</td>
															<td style="width:5px;">&nbsp;:&nbsp;</td>
															<td id="rgn-IVisit_Duration_Min_Customer_From">-</td>
														</tr>
														<tr>
															<td style="width:65px;font-weight:bold;padding-left:20px;">To</td>
															<td style="width:5px;">&nbsp;:&nbsp;</td>
															<td id="rgn-IVisit_Duration_Min_Customer_To">-</td>
														</tr>
												</table>	
											</div>		
									
									</div>		
								</div>	
								<div id="rgn-dashboard-item22" style="padding:10px;display:none;">

									<table cellspacing="0" cellpadding="4" border="0" width="99%" style="margin:10px 0px 0px 10px">
										<tr>
											<td colspan="2">
												<input type="hidden" id="inp-DASHBOARD_ITEM22_PARAM1_VALUE">
												<label for="inp-DASHBOARD_ITEM22_PARAM1_TEXT" >	
													<div><b>Region :</b></div>
													<div id="inp-rgn-DASHBOARD_ITEM22_PARAM1" style="width:300px;display:inline;">
														<input type="text" id="inp-DASHBOARD_ITEM22_PARAM1_TEXT"  class="text-225"  style="padding-right: 22px;width:265px;" readonly>
														<img class="picker inElement" id="inp-DASHBOARD_ITEM22_PARAM1-picker" src="images/icons/picker.png" style="cursor: pointer; margin-bottom: 1px;" />	
													</div>
													<div id="inp-DASHBOARD_ITEM22_PARAM1_CONTAINER" class="dropeddown-container" style="display:none;">
														<iframe id="inp-DASHBOARD_ITEM22_PARAM1_ITEMS" src="" width="100%" height="100%" frameborder="0"></iframe>
													</div>
												</label>
											</td>
										</tr>

										<tr id="rgn-DASHBOARD_ITEM4_WEEKLY1" style="display:none;" valign="bottom">
											<td style="width:110px;">
												<table ellspacing="0" cellpadding="0" border="0" width="99%" align="left">
													<tr>
														<td style="width:95px;" >
															<label for="inp-DASHBOARD_ITEM22_PARAM2">
																<div><b>Date :</b></div>
																<div style="width:130px;display:inline;">
																	<input name='date_toggled' type="text" id="inp-DASHBOARD_ITEM22_PARAM2"  class="text-65"  style="padding-right: 22px;" onclick="javascript:ce5.show();" onKeyPress='return InputSlashNumbersOnly(this, event)'>
																	<img class="picker inElement" id="inp-DASHBOARD_ITEM22_PARAM2-picker" src="images/icons/calendar.gif" style="cursor: pointer; margin-bottom: 1px;" />														
																</div>
															</label>
														</td>
													</tr>
												</table>
											</td>
											<td align="right" style="padding-right:20px;width:140px;">
												<a id="bt-DASHBOARD_ITEM22_EXPORT1" style="font-size:9px;" class="button button-blue"><span>Export</span></a>
												<a id="bt-DASHBOARD_ITEM22_PROCESS1" class="button button-blue" style="font-size:9px;"><span>Process</span></a>
												<div id="bt-DASHBOARD_ITEM22_EXPORT_CONTAINER1" class="dropeddown-container" style="display:none;margin-top:1px; height: auto;">
													<div style="color:white;background-color:#3779BA;margin:0;" class="exportItemContainer blue"> 
														<div onclick="DashboardItem22_Export_1(this);" onmouseout="javascript:evhandleExportItem_mouseout(this);" onmouseover="javascript:evhandleExportItem_mouseover(this);" class="exportItem">Chart Data</div> 
													</div>												
												</div>
											</td>
										</tr>
										<tr id="rgn-DASHBOARD_ITEM4_WEEKLY2" style="display:none;" valign="bottom">
											<td colspan="2" >
											</td>
										</tr>
										<tr id="rgn-DASHBOARD_ITEM4_MONTHLY1" style="display:none;" valign="bottom">
											<td style="width:110px;">
												<table ellspacing="0" cellpadding="0" border="0" align="left">
													<tr>
														<td style="width:95px;" >
															<label for="inp-DASHBOARD_ITEM22_PARAM3">
																<div><b>Month :</b></div>
																<select id="inp-DASHBOARD_ITEM22_PARAM3" style="width:85px">
																	<option value="0" selected="selected"></option>
																	<option value="1">January</option>
																	<option value="2">February</option>
																	<option value="3">March</option>
																	<option value="4">April</option>
																	<option value="5">May</option>
																	<option value="6">June</option>
																	<option value="7">July</option>
																	<option value="8">August</option>
																	<option value="9">September</option>
																	<option value="10">October</option>
																	<option value="11">November</option>
																	<option value="12">December</option>
																</select>															
															</label>
														</td>
														<td style="width:10px;">
														</td>
														<td style="width:95px;">
															<label for="inp-DASHBOARD_ITEM22_PARAM4">
																<div><b>Year :</b></div>
																<span id="inp-rgn-DASHBOARD_ITEM22_PARAM4"> 
																	<select id="inp-DASHBOARD_ITEM22_PARAM4" >
																		<option value="0000"></option>
																	</select>
																</span>															
															</label>
														</td>
													</tr>
												</table>
											</td>
											<td align="right" style="padding-right:20px;width:140px;">
												<a id="bt-DASHBOARD_ITEM22_EXPORT2" style="font-size:9px;" class="button button-blue"><span>Export</span></a>
												<a id="bt-DASHBOARD_ITEM22_PROCESS2" class="button button-blue" style="font-size:9px;"><span>Process</span></a>
												<div id="bt-DASHBOARD_ITEM22_EXPORT_CONTAINER2" class="dropeddown-container" style="display:none;margin-top:1px; height: auto;">
													<div style="color:white;background-color:#3779BA;margin:0;" class="exportItemContainer blue"> 
														<div onclick="DashboardItem22_Export_1(this);" onmouseout="javascript:evhandleExportItem_mouseout(this);" onmouseover="javascript:evhandleExportItem_mouseover(this);" class="exportItem">KPI Salesman</div> 
													</div>												
												</div>
											</td>
										</tr>
										<tr id="rgn-DASHBOARD_ITEM4_MONTHLY2" style="display:none;" valign="bottom">
											<td colspan="2" align="right" >
											</td>
										</tr>
									
									</table>
								
								</div>	
								<div id="rgn-dashboard-item23" style="padding:10px;display:none;">
									<table cellspacing="0" cellpadding="4" border="0" width="99%" style="margin:10px 0px 0px 10px">
										<tr>
											<td colspan="2">
												<input type="hidden" id="inp-DASHBOARD_ITEM23_PARAM1_VALUE">
												<label for="inp-DASHBOARD_ITEM23_PARAM1_TEXT" >	
													<div><b>Region :</b></div>
													<div id="inp-rgn-DASHBOARD_ITEM23_PARAM1" style="width:300px;display:inline;">
														<input type="text" id="inp-DASHBOARD_ITEM23_PARAM1_TEXT"  class="text-225"  style="padding-right: 22px;width:265px;" readonly>
														<img class="picker inElement" id="inp-DASHBOARD_ITEM23_PARAM1-picker" src="images/icons/picker.png" style="cursor: pointer; margin-bottom: 1px;" />	
													</div>
													<div id="inp-DASHBOARD_ITEM23_PARAM1_CONTAINER" class="dropeddown-container" style="display:none;">
														<iframe id="inp-DASHBOARD_ITEM23_PARAM1_ITEMS" src="" width="100%" height="100%" frameborder="0"></iframe>
													</div>
												</label>
											</td>
										</tr>
										<tr valign="bottom">
											<td align="left">
												<table ellspacing="0" cellpadding="0" border="0" align="left">
													<tr>
														<td style="width:95px;" >
															<label for="inp-DASHBOARD_ITEM23_PARAM2">
																<div><b>From :</b></div>
																<div style="width:130px;display:inline;">
																	<input name='date_toggled' type="text" id="inp-DASHBOARD_ITEM23_PARAM2"  class="text-65"  style="padding-right: 22px;" onclick="javascript:ce7.show();" onKeyPress='return InputSlashNumbersOnly(this, event)'>
																	<img class="picker inElement" id="inp-DASHBOARD_ITEM23_PARAM2-picker" src="images/icons/calendar.gif" style="cursor: pointer; margin-bottom: 1px;" />														
																</div>
															</label>
														</td>
														<td style="width:10px;">
														</td>
														<td style="width:95px;">
															<label for="inp-DASHBOARD_ITEM23_PARAM3">
																<div><b>Until :</b></div>
																<div style="width:130px;display:inline;">
																	<input name='date_toggled' type="text" id="inp-DASHBOARD_ITEM23_PARAM3"  class="text-65"  style="padding-right: 22px;" onclick="javascript:ce8.show();" onKeyPress='return InputSlashNumbersOnly(this, event)'>
																	<img class="picker inElement" id="inp-DASHBOARD_ITEM23_PARAM3-picker" src="images/icons/calendar.gif" style="cursor: pointer; margin-bottom: 1px;" />														
																</div>
															</label>
														</td>
													</tr>
												</table>										
											</td>
											<td align="center">
											</td>
										</tr>
										<tr valign="bottom">
											<td colspan="2" align="right" style="padding-right:20px;">
												<a id="bt-DASHBOARD_ITEM23_EXPORT" style="font-size:9px;" class="button button-blue"><span>Export</span></a>
												<a id="bt-DASHBOARD_ITEM23_PROCESS" class="button button-blue" style="font-size:9px;"><span>Process</span></a>
												<div id="bt-DASHBOARD_ITEM23_EXPORT_CONTAINER" class="dropeddownexp-container" style="display:none;margin-top:1px; height: auto;">
													<div style="color:white;background-color:#3779BA;margin:0;" class="exportItemContainer blue"> 
														<div onclick="DashboardItem23_Export_1(this);" onmouseout="javascript:evhandleExportItem_mouseout(this);" onmouseover="javascript:evhandleExportItem_mouseover(this);" class="exportItem">Order Calls Summary</div> 
													</div>
													<div style="color:white;background-color:#3779BA;margin:0;" class="exportItemContainer blue"> 
														<div onclick="DashboardItem23_Export_2(this);" onmouseout="javascript:evhandleExportItem_mouseout(this);" onmouseover="javascript:evhandleExportItem_mouseover(this);" class="exportItem">Order Calls Detail</div> 
													</div>												
												</div>
											</td>
										</tr>
									</table>
								</div>
								<div id="rgn-dashboard-item24" style="padding:10px;display:none;">
									<table cellspacing="0" cellpadding="4" border="0" width="99%" style="margin:10px 0px 0px 10px">
										<tr>
											<td colspan="2">
												<input type="hidden" id="inp-DASHBOARD_ITEM24_PARAM1_VALUE">
												<label for="inp-DASHBOARD_ITEM24_PARAM1_TEXT" >	
													<div><b>Region :</b></div>
													<div id="inp-rgn-DASHBOARD_ITEM24_PARAM1" style="width:300px;display:inline;">
														<input type="text" id="inp-DASHBOARD_ITEM24_PARAM1_TEXT"  class="text-225"  style="padding-right: 22px;width:265px;" readonly>
														<img class="picker inElement" id="inp-DASHBOARD_ITEM24_PARAM1-picker" src="images/icons/picker.png" style="cursor: pointer; margin-bottom: 1px;" />	
													</div>
													<div id="inp-DASHBOARD_ITEM24_PARAM1_CONTAINER" class="dropeddown-container" style="display:none;">
														<iframe id="inp-DASHBOARD_ITEM24_PARAM1_ITEMS" src="" width="100%" height="100%" frameborder="0"></iframe>
													</div>
												</label>
											</td>
										</tr>
										<tr valign="bottom">
											<td align="left">
												<table ellspacing="0" cellpadding="0" border="0" align="left">
													<tr>
														<td style="width:95px;" >
															<label for="inp-DASHBOARD_ITEM24_PARAM2">
																<div><b>From :</b></div>
																<div style="width:130px;display:inline;">
																	<input name='date_toggled' type="text" id="inp-DASHBOARD_ITEM24_PARAM2"  class="text-65"  style="padding-right: 22px;" onclick="javascript:ce12.show();" onKeyPress='return InputSlashNumbersOnly(this, event)'>
																	<img class="picker inElement" id="inp-DASHBOARD_ITEM24_PARAM2-picker" src="images/icons/calendar.gif" style="cursor: pointer; margin-bottom: 1px;" />														
																</div>
															</label>
														</td>
														<td style="width:10px;">
														</td>
														<td style="width:95px;">
															<label for="inp-DASHBOARD_ITEM24_PARAM3">
																<div><b>Until :</b></div>
																<div style="width:130px;display:inline;">
																	<input name='date_toggled' type="text" id="inp-DASHBOARD_ITEM24_PARAM3"  class="text-65"  style="padding-right: 22px;" onclick="javascript:ce13.show();" onKeyPress='return InputSlashNumbersOnly(this, event)'>
																	<img class="picker inElement" id="inp-DASHBOARD_ITEM24_PARAM3-picker" src="images/icons/calendar.gif" style="cursor: pointer; margin-bottom: 1px;" />														
																</div>
															</label>
														</td>
													</tr>
												</table>										
											</td>
											<td align="center">
											</td>
										</tr>
										<tr valign="bottom">
											<td colspan="2" align="right" style="padding-right:20px;">
												<a id="bt-DASHBOARD_ITEM24_EXPORT" style="font-size:9px;" class="button button-blue"><span>Export</span></a>
												<a id="bt-DASHBOARD_ITEM24_PROCESS" class="button button-blue" style="font-size:9px;"><span>Process</span></a>
												<div id="bt-DASHBOARD_ITEM24_EXPORT_CONTAINER" class="dropeddownexp-container" style="display:none;margin-top:1px; height: auto;">
													<div style="color:white;background-color:#3779BA;margin:0;display:none;" class="exportItemContainer blue"> 
														<div onclick="DashboardItem24_Export_1(this);" onmouseout="javascript:evhandleExportItem_mouseout(this);" onmouseover="javascript:evhandleExportItem_mouseover(this);" class="exportItem">NOO Summary</div> 
													</div>
													<div style="color:white;background-color:#3779BA;margin:0;" class="exportItemContainer blue"> 
														<div onclick="DashboardItem24_Export_2(this);" onmouseout="javascript:evhandleExportItem_mouseout(this);" onmouseover="javascript:evhandleExportItem_mouseover(this);" class="exportItem">NOO Detail</div> 
													</div>												
												</div>
											</td>
										</tr>
									</table>
								</div>
								<div id="rgn-dashboard-item25" style="padding:10px;display:none;">
									<table cellspacing="0" cellpadding="4" border="0" width="99%" style="margin:10px 0px 0px 10px">
										<tr>
										</tr>
									</table>
								</div>
															
							</div>
							<ul id="dashboard-switcher" class="dashboardselector" style="position:absolute; width:40px;padding:0px 0px 0px 3px;margin: 8px 0px 0px 0px;">
								<li id="dashboard-switcher-tab21" class="item21 inactive" onclick="gTips_hide(this)" onmouseout="if(DasboardItem2TabState!=1) gTips_hide(this)" onmouseover="if(DasboardItem2TabState!=1) gTips_showd(this)" gtip="Taking Order" desc="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."></li>
								<li id="dashboard-switcher-tab22" class="item22 inactive" onclick="gTips_hide(this)" onmouseout="if(DasboardItem2TabState!=2) gTips_hide(this)" onmouseover="if(DasboardItem2TabState!=2) gTips_showd(this)" gtip="KPI Salesman" desc="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."></li>
								<li id="dashboard-switcher-tab23" class="item23 inactive" onclick="gTips_hide(this)" onmouseout="if(DasboardItem2TabState!=3) gTips_hide(this)" onmouseover="if(DasboardItem2TabState!=3) gTips_showd(this)" gtip="Order Calls" desc="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."></li>
								<li id="dashboard-switcher-tab24" class="item24 inactive" onclick="gTips_hide(this)" onmouseout="if(DasboardItem2TabState!=4) gTips_hide(this)" onmouseover="if(DasboardItem2TabState!=4) gTips_showd(this)" gtip="NOO" desc="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."></li>
								<li id="dashboard-switcher-tab25" class="item25 inactive" onclick="gTips_hide(this)" onmouseout="if(DasboardItem2TabState!=5) gTips_hide(this)" onmouseover="if(DasboardItem2TabState!=5) gTips_showd(this)" gtip="Reserve" style="visibility:hidden;" desc="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."></li>
							</ul>
					</div>										
					<!-- CONTENT -- END --->	
					<div id="map-sidebar-tab3-content-footer" style="background-color:#fff;right:0px; left:43px; position:absolute; bottom:0px; height:19px; border-top:1px solid #E0E0E0; display:none; ">
						<div id="map-sidebar-tab3-content-footer-pagingcontrol" style="width:auto; height:20px; font-family:Arial;font-size:10px;text-align: center; padding:5px 5px 5px 5px ;display:none;">
							<a id="bt-tab3-nav-first-1" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">&laquo; First</a>&nbsp;
							<a id="bt-tab3-nav-prev-1" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">&lsaquo; Previous</a>&nbsp;
							<span id="bt-tab3-nav-current-1" style="font-size:11px; display:inline;"></span>&nbsp;
							<a id="bt-tab3-nav-next-1" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">Next &rsaquo;</a>&nbsp;
							<a id="bt-tab3-nav-last-1" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">Last &raquo;</a>&nbsp;  

							<div style="display:none;">	
								<a id="bt-tab3-nav-first-2" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">&laquo; First</a>&nbsp;
								<a id="bt-tab3-nav-prev-2" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">&lsaquo; Previous</a>&nbsp;
								<span id="bt-tab3-nav-current-2" style="font-size:11px; display:inline;"></span>&nbsp;
								<a id="bt-tab3-nav-next-2" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">Next &rsaquo;</a>&nbsp;
								<a id="bt-tab3-nav-last-2" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">Last &raquo;</a>&nbsp;                            	
							</div>					
						</div>					
					</div>					
				</div>	
				
				<!-- customer -->
				<div id="map-sidebar-tab4-container" style="clear: both; display: none; border-bottom:1px solid #E0E0E0;background-color:#fff; height: 50px;" class="br-content">
					<div id="map-sidebar-tab4-content-controls" style="padding:3px;">					
						
						<div id="rgn-dashboarditem3-description" style="visibility:hidden;color:silver;font-size:11px;margin-top:2px;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </div>
					</div>
					<!-- CONTENT -- BEGIN --->						
					<div id="map-sidebar-tab4-content-items" style="width:100%; position:absolute; top:58px; right:0px; bottom:0px; overflow-x:hidden; overflow-y:auto;">
							<div style="background-color:#fff;border-color:#eaeaea;left:42px;top:0px; right:0px; bottom:0px; border-style:solid;border-width:1px 0px 0px 1px;padding:0;position:absolute; margin: 0;">							
								<div id="rgn-dashboard-item31" style="padding:10px;display:none;">

									<table cellspacing="0" cellpadding="4" border="0" width="99%" style="margin:10px 0px 0px 10px">
										<tr>
											<td colspan="2">
												<input type="hidden" id="inp-DASHBOARD_ITEM31_PARAM1_VALUE">
												<label for="inp-DASHBOARD_ITEM31_PARAM1_TEXT" >	
													<div><b>Product :</b></div>
													<div id="inp-rgn-DASHBOARD_ITEM31_PARAM1" style="width:300px;display:inline;">
														<input type="text" id="inp-DASHBOARD_ITEM31_PARAM1_TEXT"  class="text-225"  style="padding-right: 22px;width:265px;" readonly>
														<img class="picker inElement" id="inp-DASHBOARD_ITEM31_PARAM1-picker" src="images/icons/picker.png" style="cursor: pointer; margin-bottom: 1px;" />	
													</div>
													<div id="inp-DASHBOARD_ITEM31_PARAM1_CONTAINER" class="dropeddown-container" style="display:none;height: 250px;">
														<iframe id="inp-DASHBOARD_ITEM31_PARAM1_ITEMS" src="" width="100%" height="100%" frameborder="0"></iframe>
													</div>
												</label>
											</td>
										</tr>
										<tr valign="bottom">
											<td align="left" colspan="2">
												<label for="inp-DASHBOARD_ITEM31_PARAM2" >	
													<div><b>Period :</b></div>								
													<select id="inp-DASHBOARD_ITEM31_PARAM2">
														<option value="0"></option>
														<option value="1">January</option>
														<option value="2">February</option> 
														<option value="3">March</option> 
														<option value="4">April</option> 
														<option value="5">May</option> 
														<option value="6">June</option> 
														<option value="7">July</option> 
														<option value="8">August</option> 
														<option value="9">September</option> 
														<option value="10">October</option> 
														<option value="11">November</option> 
														<option value="12">December</option> 
													</select>
												</label>	
												<div id="inp-rgn-DASHBOARD_ITEM31_PARAM3" style="display:inline;">
												</div>										
											</td>
										</tr>
										<tr valign="bottom">
											<td align="left">
												<label for="inp-DASHBOARD_ITEM31_PARAM4" >	
													<div><b>Calculate by :</b></div>								
													<select id="inp-DASHBOARD_ITEM31_PARAM4">
														<option value="0">-</option>
														<option value="1">Order Amount</option>
														<option value="2">Order Quantity</option>
													</select>
												</label>											
											</td>
											<td align="right">
											</td>
										</tr>
										<tr valign="bottom">
											<td colspan="2" align="right" style="padding-right:20px;">
												<a id="bt-DASHBOARD_ITEM31_EXPORT" style="font-size:9px;" class="button button-blue"><span>Export</span></a>
												<a id="bt-DASHBOARD_ITEM31_PROCESS" class="button button-blue" style="font-size:9px;"><span>Process</span></a>
												<div id="bt-DASHBOARD_ITEM31_EXPORT_CONTAINER" class="dropeddownexp-container" style="display:none;margin-top:1px; height: auto;">
													<div style="color:white;background-color:#3779BA;margin:0;" class="exportItemContainer blue"> 
														<div onclick="DashboardItem31_Export_1(this);" onmouseout="javascript:evhandleExportItem_mouseout(this);" onmouseover="javascript:evhandleExportItem_mouseover(this);" class="exportItem">Top 10 Summary</div> 
													</div>
													<div style="color:white;background-color:#3779BA;margin:0;" class="exportItemContainer blue"> 
														<div onclick="DashboardItem31_Export_2(this);" onmouseout="javascript:evhandleExportItem_mouseout(this);" onmouseover="javascript:evhandleExportItem_mouseover(this);" class="exportItem">Top 10 Detail</div> 
													</div>												
												</div>
											</td>
										</tr>
									</table>
								
								</div>	
								<div id="rgn-dashboard-item32" style="padding:10px;display:none;">

									<table cellspacing="0" cellpadding="4" border="0" width="99%" style="margin:10px 0px 0px 10px">
										<tr>
											<td colspan="2">
												<input type="hidden" id="inp-DASHBOARD_ITEM32_PARAM1_VALUE">
												<input type="hidden" id="inp-DASHBOARD_ITEM32_PARAM1_TEXT_DUMMY">
												<label for="inp-DASHBOARD_ITEM32_PARAM1_TEXT" >	
													<div><b>Search Customer :</b></div>
													<div style="width:300px;display:inline;">
														<input type="text" id="inp-DASHBOARD_ITEM32_PARAM1_TEXT"  class="text-225"  style="padding-right: 22px;width:265px;" >
													</div>
													<div id="inp-DASHBOARD_ITEM32_PARAM1_CONTAINER" style="display:none;width:265px;">
													</div>
												</label>							
												<div style="margin-right:16px;margin-top:5px;display:none;" id="inp-rgn-info-DASHBOARD_ITEM32_PARAM1" >
													<div style="font-size:11px;">
														<span><b>Name:</b></span>&nbsp;<span id="inp-rgn-info-DASHBOARD_ITEM32_PARAM1-CUSTOMER_NAME"></span>
													</div>
													<div style="font-size:10px;">
														<span><b>ID:</b></span>&nbsp;<span id="inp-rgn-info-DASHBOARD_ITEM32_PARAM1-CUSTOMER_ID"></span>
													</div>
													<div style="font-size:10px;">
														<span><b>Address:</b></span>&nbsp;<span id="inp-rgn-info-DASHBOARD_ITEM32_PARAM1-CUSTOMER_ADDRESS"></span>
													</div>
													
												</div>
											</td>
										</tr>
										<tr valign="bottom">
											<td align="left">
												<table ellspacing="0" cellpadding="0" border="0" align="left">
													<tr>
														<td style="width:111px;" >
															<label for="inp-DASHBOARD_ITEM32_PARAM2">
																<div><b>From :</b></div>
																<div style="width:130px;display:inline;">
																	<input name='date_toggled' type="text" id="inp-DASHBOARD_ITEM32_PARAM2"  class="text-65"  style="padding-right: 22px;" onclick="javascript:ce10.show();" onKeyPress='return InputSlashNumbersOnly(this, event)'>
																	<img class="picker inElement" id="inp-DASHBOARD_ITEM32_PARAM2-picker" src="images/icons/calendar.gif" style="cursor: pointer; margin-bottom: 1px;" />														
																</div>
															</label>
														</td>
														<td style="width:10px;">
														</td>
														<td style="width:111px;">
															<label for="inp-DASHBOARD_ITEM32_PARAM3">
																<div><b>Until :</b></div>
																<div style="width:130px;display:inline;">
																	<input name='date_toggled' type="text" id="inp-DASHBOARD_ITEM32_PARAM3"  class="text-65"  style="padding-right: 22px;" onclick="javascript:ce11.show();" onKeyPress='return InputSlashNumbersOnly(this, event)'>
																	<img class="picker inElement" id="inp-DASHBOARD_ITEM32_PARAM3-picker" src="images/icons/calendar.gif" style="cursor: pointer; margin-bottom: 1px;" />														
																</div>
															</label>
														</td>
													</tr>
												</table>										
											</td>
											<td align="center">
											</td>
										</tr>	
										<tr valign="bottom">
											<td colspan="2" align="right" style="padding-right:20px;">
												<a id="bt-DASHBOARD_ITEM32_EXPORT" style="font-size:9px;" class="button button-blue"><span>Export</span></a>
												<a id="bt-DASHBOARD_ITEM32_PROCESS" class="button button-blue" style="font-size:9px;"><span>Process</span></a>
												<div id="bt-DASHBOARD_ITEM32_EXPORT_CONTAINER" class="dropeddownexp-container" style="display:none;margin-top:1px; height: auto;">
													<div style="color:white;background-color:#3779BA;margin:0;" class="exportItemContainer blue"> 
														<div onclick="DashboardItem32_Export_1(this);" onmouseout="javascript:evhandleExportItem_mouseout(this);" onmouseover="javascript:evhandleExportItem_mouseover(this);" class="exportItem">Customer Order Summary</div> 
													</div>
													<div style="color:white;background-color:#3779BA;margin:0;" class="exportItemContainer blue"> 
														<div onclick="DashboardItem32_Export_2(this);" onmouseout="javascript:evhandleExportItem_mouseout(this);" onmouseover="javascript:evhandleExportItem_mouseover(this);" class="exportItem">Customer Order Detail</div> 
													</div>												
												</div>
											</td>
										</tr>									
									</table>
								
								</div>	
								<div id="rgn-dashboard-item33" style="padding:10px;display:none;">

									<table cellspacing="0" cellpadding="4" border="0" width="99%" style="margin:10px 0px 0px 10px">
										<tr>
										</tr>
									</table>
								
								</div>	
							</div>
							<ul id="dashboard-switcher" class="dashboardselector" style="position:absolute; width:40px;padding:0px 0px 0px 3px;margin: 8px 0px 0px 0px;">
								<li id="dashboard-switcher-tab31" class="item31 inactive" onclick="gTips_hide(this)" onmouseout="if(DasboardItem3TabState!=1) gTips_hide(this)" onmouseover="if(DasboardItem3TabState!=1) gTips_showd(this)" gtip="Top 10 Customer" desc="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."></li>
								<li id="dashboard-switcher-tab32" class="item32 inactive" onclick="gTips_hide(this)" onmouseout="if(DasboardItem3TabState!=2) gTips_hide(this)" onmouseover="if(DasboardItem3TabState!=2) gTips_showd(this)" gtip="Customer Order" desc="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."></li>
								<li id="dashboard-switcher-tab33" class="item33 inactive" onclick="gTips_hide(this)" onmouseout="if(DasboardItem3TabState!=3) gTips_hide(this)" onmouseover="if(DasboardItem3TabState!=3) gTips_showd(this)" gtip="Reserve" style="visibility:hidden;" desc="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."></li>
							</ul>
					</div>										
					<!-- CONTENT -- END --->	
					<div id="map-sidebar-tab4-content-footer" style="background-color:#fff;width:100%;right:0px; position:absolute; bottom:0px; height:19px; border-top:1px solid #E0E0E0; display:none; ">
						<div style="width:auto; height:20px; font-family:Arial;font-size:10px;text-align: center; padding:5px 5px 5px 5px ;display:none;">
							<a id="bt-tab4-nav-first-1" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">&laquo; First</a>&nbsp;
							<a id="bt-tab4-nav-prev-1" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">&lsaquo; Previous</a>&nbsp;
							<span id="bt-tab4-nav-current-1" style="font-size:11px; display:inline;"></span>&nbsp;
							<a id="bt-tab4-nav-next-1" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">Next &rsaquo;</a>&nbsp;
							<a id="bt-tab4-nav-last-1" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">Last &raquo;</a>&nbsp;  

							<div style="display:none;">	
								<a id="bt-tab4-nav-first-2" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">&laquo; First</a>&nbsp;
								<a id="bt-tab4-nav-prev-2" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">&lsaquo; Previous</a>&nbsp;
								<span id="bt-tab4-nav-current-2" style="font-size:11px; display:inline;"></span>&nbsp;
								<a id="bt-tab4-nav-next-2" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">Next &rsaquo;</a>&nbsp;
								<a id="bt-tab4-nav-last-2" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">Last &raquo;</a>&nbsp;                            	
							</div>					
						</div>					
					</div>					
				</div>	
				
				<!-- stock -->
				<div id="map-sidebar-tab5-container" style="clear: both; display: none; border-bottom:1px solid #E0E0E0;background-color:#fff; height: 50px;" class="br-content">
					<div id="map-sidebar-tab5-content-controls" style="padding:3px;">						
						
						<div id="rgn-dashboarditem4-description" style="visibility:hidden;color:silver;font-size:11px;margin-top:2px;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </div>
					</div>
					<!-- CONTENT -- BEGIN --->						
					<div id="map-sidebar-tab5-content-items" style="width:100%; position:absolute; top:58px; right:0px; bottom:0px; overflow-x:hidden; overflow-y:auto;">
							<div style="background-color:#fff;border-color:#eaeaea;left:42px;top:0px; right:0px; bottom:0px; border-style:solid;border-width:1px 0px 0px 1px;padding:0;position:absolute; margin: 0;">							
								<div id="rgn-dashboard-item41" style="padding:10px;display:none;">
										
									<table cellspacing="0" cellpadding="4" border="0" width="99%" style="margin:10px 0px 0px 10px">
										<tr>
											<td colspan="2">
												<input type="hidden" id="inp-DASHBOARD_ITEM41_PARAM1_VALUE">
												<label for="inp-DASHBOARD_ITEM41_PARAM1_TEXT" >	
													<div><b>Product :</b></div>
													<div id="inp-rgn-DASHBOARD_ITEM41_PARAM1" style="width:300px;display:inline;">
														<input type="text" id="inp-DASHBOARD_ITEM41_PARAM1_TEXT"  class="text-225"  style="padding-right: 22px;width:265px;" readonly>
														<img class="picker inElement" id="inp-DASHBOARD_ITEM41_PARAM1-picker" src="images/icons/picker.png" style="cursor: pointer; margin-bottom: 1px;" />	
													</div>
													<div id="inp-DASHBOARD_ITEM41_PARAM1_CONTAINER" class="dropeddown-container" style="display:none;">
														<iframe id="inp-DASHBOARD_ITEM41_PARAM1_ITEMS" src="" width="100%" height="100%" frameborder="0"></iframe>
													</div>
												</label>
											</td>
										</tr>
										<tr valign="bottom">
											<td colspan="2" align="right" style="padding-right:20px;">
												<a id="bt-DASHBOARD_ITEM41_EXPORT" style="font-size:9px;" class="button button-blue"><span>Export</span></a>
												<a id="bt-DASHBOARD_ITEM41_PROCESS" class="button button-blue" style="font-size:9px;"><span>Process</span></a>
												<div id="bt-DASHBOARD_ITEM41_EXPORT_CONTAINER" class="dropeddownexp-container" style="display:none;margin-top:1px; height: auto;">
													<div style="color:white;background-color:#3779BA;margin:0;" class="exportItemContainer blue"> 
														<div onclick="DashboardItem41_Export_1(this);" onmouseout="javascript:evhandleExportItem_mouseout(this);" onmouseover="javascript:evhandleExportItem_mouseover(this);" class="exportItem">Customer's Stock</div> 
													</div>												
												</div>
											</td>
										</tr>
									</table>	
											
								</div>	
								<div id="rgn-dashboard-item42" style="padding:10px;display:none;">

									<table cellspacing="0" cellpadding="4" border="0" width="99%" style="margin:10px 0px 0px 10px">
										<tr>
											<td colspan="2">
												<input type="hidden" id="inp-DASHBOARD_ITEM42_PARAM1_VALUE">
												<label for="inp-DASHBOARD_ITEM42_PARAM1_TEXT" >	
													<div><b>Region :</b></div>
													<div id="inp-rgn-DASHBOARD_ITEM42_PARAM1" style="width:300px;display:inline;">
														<input type="text" id="inp-DASHBOARD_ITEM42_PARAM1_TEXT"  class="text-225"  style="padding-right: 22px;width:265px;" readonly>
														<img class="picker inElement" id="inp-DASHBOARD_ITEM42_PARAM1-picker" src="images/icons/picker.png" style="cursor: pointer; margin-bottom: 1px;" />	
													</div>
													<div id="inp-DASHBOARD_ITEM42_PARAM1_CONTAINER" class="dropeddown-container" style="display:none;">
														<iframe id="inp-DASHBOARD_ITEM42_PARAM1_ITEMS" src="" width="100%" height="100%" frameborder="0"></iframe>
													</div>
												</label>
											</td>
										</tr>
										<tr valign="bottom">
											<td align="left" >
												<label for="inp-DASHBOARD_ITEM42_PARAM2" >	
													<div><b>Customer Type :</b></div>
													<div id="inp-rgn-DASHBOARD_ITEM42_PARAM2" style="display:inline;">
													</div>
												</label>
											</td>
											<td align="right" style="padding-right:20px;">
												<a id="bt-DASHBOARD_ITEM42_EXPORT" style="font-size:9px;display:none;" class="button button-blue"><span>Export</span></a>
												<a id="bt-DASHBOARD_ITEM42_PROCESS" class="button button-blue" style="font-size:9px;"><span>Process</span></a>
												<div id="bt-DASHBOARD_ITEM42_EXPORT_CONTAINER" class="dropeddownexp-container" style="display:none;margin-top:1px; height: auto;">
													<div style="color:white;background-color:#3779BA;margin:0;" class="exportItemContainer blue"> 
														<div onclick="DashboardItem42_Export_1(this);" onmouseout="javascript:evhandleExportItem_mouseout(this);" onmouseover="javascript:evhandleExportItem_mouseover(this);" class="exportItem">Chart Data</div> 
													</div>												
												</div>
											</td>
										</tr>
									</table>
								
								</div>	
								<div id="rgn-dashboard-item43" style="padding:10px;display:none;">

									<table cellspacing="0" cellpadding="4" border="0" width="99%" style="margin:10px 0px 0px 10px">
										
									</table>
								
								</div>	
							</div>
							<ul id="dashboard-switcher" class="dashboardselector" style="position:absolute; width:40px;padding:0px 0px 0px 3px;margin: 8px 0px 0px 0px;">
								<li id="dashboard-switcher-tab41" class="item41 inactive" onclick="gTips_hide(this)" onmouseout="if(DasboardItem4TabState!=1) gTips_hide(this)" onmouseover="if(DasboardItem4TabState!=1) gTips_showd(this)" gtip="Customer's Stock" desc="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."></li>
								<li id="dashboard-switcher-tab42" class="item42 inactive" onclick="gTips_hide(this)" onmouseout="if(DasboardItem4TabState!=2) gTips_hide(this)" onmouseover="if(DasboardItem4TabState!=2) gTips_showd(this)" gtip="Customer's Stock 2" desc="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."></li>
								<li id="dashboard-switcher-tab43" class="item43 inactive" onclick="gTips_hide(this)" onmouseout="if(DasboardItem4TabState!=3) gTips_hide(this)" onmouseover="if(DasboardItem4TabState!=3) gTips_showd(this)" gtip="Reserve" style="visibility:hidden;" desc="Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."></li>
							</ul>
					</div>										
					<!-- CONTENT -- END --->	
					<div id="map-sidebar-tab5-content-footer" style="background-color:#fff;width:100%;right:0px; position:absolute; bottom:0px; height:19px; border-top:1px solid #E0E0E0; display:none; ">
						<div style="width:auto; height:20px; font-family:Arial;font-size:10px;text-align: center; padding:5px 5px 5px 5px ;display:none;">
							<a id="bt-tab5-nav-first-1" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">&laquo; First</a>&nbsp;
							<a id="bt-tab5-nav-prev-1" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">&lsaquo; Previous</a>&nbsp;
							<span id="bt-tab5-nav-current-1" style="font-size:11px; display:inline;"></span>&nbsp;
							<a id="bt-tab5-nav-next-1" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">Next &rsaquo;</a>&nbsp;
							<a id="bt-tab5-nav-last-1" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">Last &raquo;</a>&nbsp;  

							<div style="display:none;">	
								<a id="bt-tab5-nav-first-2" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">&laquo; First</a>&nbsp;
								<a id="bt-tab5-nav-prev-2" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">&lsaquo; Previous</a>&nbsp;
								<span id="bt-tab5-nav-current-2" style="font-size:11px; display:inline;"></span>&nbsp;
								<a id="bt-tab5-nav-next-2" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">Next &rsaquo;</a>&nbsp;
								<a id="bt-tab5-nav-last-2" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">Last &raquo;</a>&nbsp;                            	
							</div>					
						</div>					
					</div>					
				</div>	
					
				<div id="map-sidebar-tabMeasureDist-container" style="clear: both; display: none; border-bottom:1px solid #E0E0E0; " class="br-content">
					<div id="map-sidebar-tabMeasureDist-content-controls" style="padding:6px;">
						<span style="margin-top:0px;"><h4>Distance Measurement Tool</h4></span>	
						<div class="br-full" style="margin-top:5px;margin-bottom:5px; margin-right:12px;background-color:#FFFAD4;">
							<div class="br-content">
								<img src="images/icons/sm-faq.gif" style="float:left;">
								<div style="margin-left:24px;font-size:11px;color:#494949;">Click on the map to trace a path you want to measure</div>													
								<div style="margin-left:24px;font-size:11px;color:#494949;">Double-Click on the map to stop measurement.</div>													
							</div>
						</div>						
					</div>
					<!-- CONTENT -- BEGIN --->						
					<div id="map-sidebar-tabMeasureDist-content-items" style="display:block; width:95%; position:absolute; top:120px; right:0px; bottom:20px; overflow:hidden;">
						<div style="padding:6px;">
							<div style="margin-top:10px;" id="inp-MeasureDist-caption"><h3>Total Distance:</h3></div>
							<div style="margin-top:10px;" id="inp-MeasureDist-value"><h3></h3></div>
						</div>
					</div>									
					<!-- CONTENT -- END --->	
					<div id="map-sidebar-tabMeasureDist-content-footer" style="width:100%;right:0px; position:absolute; bottom:0px; height:19px; border-top:1px solid #E0E0E0; ">
						<div style="width:auto; height:20px; font-family:Arial;font-size:10px;text-align: center; padding:5px 5px 5px 5px ;">
											
						</div>					
					</div>					
				</div>	
				
				<div id="map-sidebar-tabMeasureArea-container" style="clear: both; display: none; border-bottom:1px solid #E0E0E0; " class="br-content">
					<div id="map-sidebar-tabMeasureArea-content-controls" style="padding:6px;">
						<span style="margin-top:0px;"><h4>Area Measurement Tool</h4></span>
						<div class="br-full" style="margin-top:5px;margin-bottom:5px; margin-right:12px;background-color:#FFFAD4;">
							<div class="br-content">
								<img src="images/icons/sm-faq.gif" style="float:left;">
								<div style="margin-left:24px;font-size:11px;color:#494949;">Click on the map to trace a path you want to measure</div>													
								<div style="margin-left:24px;font-size:11px;color:#494949;">Double-Click on the map to stop measurement.</div>													
							</div>
						</div>	
					</div>
					<!-- CONTENT -- BEGIN --->						
					<div id="map-sidebar-tabMeasureArea-content-items" style="display:block; width:95%; position:absolute; top:120px; right:0px; bottom:20px; overflow:hidden;">
						<div style="padding:6px;">
							<div style="margin-top:10px;" id="inp-MeasureArea-caption"><h3>Total Area:</h3></div>
							<div style="margin-top:10px;" id="inp-MeasureArea-value"><h3></h3></div>
						</div>
					</div>									
					<!-- CONTENT -- END --->	
					<div id="map-sidebar-tabMeasureArea-content-footer" style="width:100%;right:0px; position:absolute; bottom:0px; height:19px; border-top:1px solid #E0E0E0; ">
						<div style="width:auto; height:20px; font-family:Arial;font-size:10px;text-align: center; padding:5px 5px 5px 5px ;">
											
						</div>					
					</div>					
				</div>
				
				<div id="map-sidebar-SearchLocation-container" style="clear: both; display: none; border-bottom:1px solid #E0E0E0; " class="br-content">
					<div id="map-sidebar-SearchLocation-content-controls" style="padding:6px;">
						<span style="margin-top:0px;"><h4> Lokasi SPBU</h4></span>
							<a style="font-size:11px; color:blue; cursor:pointer;" class="tabcontentLink" id="bt-SearchLocation-refresh"><span>Refresh</span></a>
					</div>
					<!-- CONTENT -- BEGIN --->						
					<div id="map-sidebar-SearchLocation-content-items" style="display:block; width:95%; position:absolute; top:60px; right:0px; bottom:20px; overflow-x:hidden; overflow-y:auto;">

					</div>									
					<!-- CONTENT -- END --->	
					<div id="map-sidebar-SearchLocation-content-footer" style="width:100%;right:0px; position:absolute; bottom:0px; height:19px; border-top:1px solid #E0E0E0; ">
						<div style="width:auto; height:20px; font-family:Arial;font-size:10px;text-align: center; padding:5px 5px 5px 5px ;">
							<a id="bt-SearchLocation-nav-first-1" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">&laquo; First</a>&nbsp;
							<a id="bt-SearchLocation-nav-prev-1" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">&lsaquo; Previous</a>&nbsp;
							<span id="bt-SearchLocation-nav-current-1" style="font-size:11px; display:inline;"></span>&nbsp;
							<a id="bt-SearchLocation-nav-next-1" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">Next &rsaquo;</a>&nbsp;
							<a id="bt-SearchLocation-nav-last-1" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">Last &raquo;</a>&nbsp;  

							<div style="display:none;">	
								<a id="bt-SearchLocation-nav-first-2" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">&laquo; First</a>&nbsp;
								<a id="bt-SearchLocation-nav-prev-2" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">&lsaquo; Previous</a>&nbsp;
								<span id="bt-SearchLocation-nav-current-2" style="font-size:11px; display:inline;"></span>&nbsp;
								<a id="bt-SearchLocation-nav-next-2" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">Next &rsaquo;</a>&nbsp;
								<a id="bt-SearchLocation-nav-last-2" class="tabcontentLink" style="font-size:11px; color:blue; cursor:pointer;display:inline;">Last &raquo;</a>&nbsp;                            	
							</div>					
						</div>					
					</div>					
				</div>	
												
			</div>
			
			<div  id="map-sidebar-footer" style="display:none;">
				<!-- nothing -->
			</div>		
	
		</div>
		<div id="map-sidebar-slide-toggle-show" style="top:125px;">
			<img src="images/icons/slide-side-show.png" id="slide-toggle-img-show" style="margin:2px 2px 2px 3px;"/>
		</div>	
		

		<div id="map-bottombar-shadow" style="display:none;" class="horzshadow">
			
		</div>
		<div id="map-bottombar" style="display:none;">
			<div id="map-bottombar-header" style="background-color:white; height:18px;position: absolute;z-index: 22">
				<div class="br-modf-head" style="background-color:white; height:26px;border-bottom:0px;">
					
				</div>	
				<div id="map-bottombar-slide-toggle-hide" style="width:32px;">
					<img id="slide-toggle-img-hide2" src="images/icons/slide-bottom-hide.png" style="margin:2px 2px 2px 10px;"/>
					<img id="slide-toggle-img-hiding2" src="images/icons/slide-bottom-show.png" style="display:none;" style="margin:2px 2px 2px 10px;"/>
				</div>
			</div>
			<div id="rgn-bottombar-item11" style="display:none;" class="bottombar-content">

			</div>			
			<div id="rgn-bottombar-item12" style="display:none;" class="bottombar-content">

			</div>		
			<div id="rgn-bottombar-item13" style="display:none;" class="bottombar-content">

			</div>				
			<div id="rgn-bottombar-item14" style="display:none;" class="bottombar-content">

			</div>			
			<div id="rgn-bottombar-item15" style="display:none;" class="bottombar-content">

			</div>		
			<div id="rgn-bottombar-item21" style="display:none;" class="bottombar-content">
				<iframe id="inp-DASHBOARD_ITEM21_SCH" name="inp-DASHBOARD_ITEM21_SCH" src="" width="100%" height="100%" frameborder="0"></iframe>
			</div>	
			<div id="rgn-bottombar-item22" style="display:none;" class="bottombar-content">

			</div>	
			<div id="rgn-bottombar-item23" style="display:none;" class="bottombar-content">

			</div>			
			<div id="rgn-bottombar-item24" style="display:none;" class="bottombar-content">

			</div>		
			<div id="rgn-bottombar-item25" style="display:none;" class="bottombar-content">

			</div>			
			<div id="rgn-bottombar-item31" style="display:none;" class="bottombar-content">

			</div>		
			<div id="rgn-bottombar-item32" style="display:none;" class="bottombar-content">

			</div>	
			<div id="rgn-bottombar-item33" style="display:none;" class="bottombar-content">

			</div>		
			<div id="rgn-bottombar-item41" style="display:none;" class="bottombar-content">

			</div>		
			<div id="rgn-bottombar-item42" style="display:none;" class="bottombar-content">

			</div>	
			<div id="rgn-bottombar-item43" style="display:none;" class="bottombar-content">

			</div>		
		</div>
	
		<div id="map-bottombar-slide-toggle-show" style="display:none;width:38px;">
			<img src="images/icons/slide-bottom-show.png" id="slide-toggle-img-show2" style="margin:2px 2px 2px 12px;"/>
		</div>	
		
		
		<div id="maptool-measure-items" style="display:none;position: absolute; left: 156px; z-index: 1500; top: 111px;">
			<div>
				<ul class="maptoolselector" style="top:5px;">
					<li id="maptool-measure-dist" class="dropdownitem" style="top:6px;"><img src="images/map-nav/mActionMeasureLength.png"/><span>&nbsp;&nbsp;&nbsp;&nbsp;Measure Distance</span></li>
					<li id="maptool-measure-area" class="dropdownitem" style="top:32px;"><img src="images/map-nav/mActionMeasureArea.png"/><span>&nbsp;&nbsp;&nbsp;&nbsp;Measure Area</span></li>
				</ul>
			</div>
		</div>	
		<div id="maptool-bookmark-items" class="maptoolbookmark" style="background:#FCF4F4; display:none; position:absolute;left:402px;font-size:8pt;z-index:1502;top:115px; height:200px;width:200px; border-style:solid; border-width:0px 1px 1px 1px; border-color:#A5A5A5;">
			<div id="maptool-bookmark-items-content-header" onmouseover="javascript:evhandleBookmark_mouseover();" onmouseout="javascript:evhandleBookmark_mouseout();" style="width:100%;right:0px; position:absolute; top:0px; height:24px; border-top:1px solid #E0E0E0; ">
				<div style="padding:3px;" onmouseover="javascript:evhandleBookmark_mouseover();" onmouseout="javascript:evhandleBookmark_mouseout();">
					<img id="bt-bookmark-refresh" src="images/icons/refresh-sm.png" style="margin-top:1px;float:left;cursor:pointer;" onmouseover="javascript:evhandleBookmark_mouseover();" onmouseout="javascript:evhandleBookmark_mouseout();"/>				
					<img id="bt-bookmark-add" src="images/map-nav/down_plus.png" style="float:right;margin-top:2px;cursor:pointer;" onmouseover="javascript:evhandleBookmark_mouseover();" onmouseout="javascript:evhandleBookmark_mouseout();"/>
					<input id="inp-MAP_BOOKMARK_NAME" value="" style="float:right;width:140px;font-size:8pt;margin-right:5px;" onmouseover="javascript:evhandleBookmark_mouseover();" onmouseout="javascript:evhandleBookmark_mouseout();" onkeypress="return dg1__AvoidPostPage(this, event)">					
				</div>
			</div>					
			<!-- max 30 char-->
			<div id="maptool-bookmark-items-content" onmouseover="javascript:evhandleBookmark_mouseover();" onmouseout="javascript:evhandleBookmark_mouseout();" style="padding:2px;display:block; width:96%; position:absolute; top:25px; right:0px; bottom:1px; overflow-y:scroll;overflow-h:hidden;background:#FCF4F4;">
			</div>
				
		</div>	
				
		<script>
			
			// global app configuration object
			var mapdashboard_getByPage= "{{ URL::to('mapdashboard_getByPage') }}" ;
			var sourceDataWorld_cities = "{{ URL('../resources/views/module/mapdashboard/data/world_cities.json') }}" ;
			/*
			var mapdashboard_AjaxIns= "{{ URL::to('mapdashboard_ins') }}" ;
			var mapdashboard_AjaxUpd= "{{ URL::to('mapdashboard_upd') }}" ;
			var mapdashboard_AjaxDel= "{{ URL::to('mapdashboard_del') }}" ;
			*/
		</script>

		<script src="{{ url('../resources/views/module/mapdashboard/mapdashboard.js') }}" type="text/javascript"></script>
		<link rel="stylesheet" type="text/css" media="all" href="{{url('../resources/views/module/mapdashboard/mapdashboard.css')}}" />
		
	
		
		
@stop()

