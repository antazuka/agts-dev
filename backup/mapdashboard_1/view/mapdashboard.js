var dgbox1_Title;
var SideBarSlideFx;
var BottomBarMainTabState;

var OpenLayers_Control_PanZoomBar_Id=".ol-zoom";
var OpenLayers_Control_ScaleLine_Id="OpenLayers_Control_ScaleLine";
var OpenLayers_Control_LayerSwitcher_Id="OpenLayers_Control_LayerSwitcher";
var OpenLayers_Control_OverviewMap_Id="OpenLayers_Control_OverviewMap";


bodyscroll = new Fx.Scroll(document.body, {
		wait: false,
		duration: 500,
		offset: {'x': 0, 'y': 0},
		transition: Fx.Transitions.Quart.easeOut 
	});	
document.title = 'AGTS - '+ dgbox1_Title;
InitialComponent();


function InitialComponent()
{
	$('bt-search-go').addEvent('click', function(e){	
		dg1__SwithcUIModf(0);
		dg1__Refresh(1);
	});
	$('bt-reset-go').addEvent('click', function(e){	
		$('inp-dgbox1-normal-search-q').set('value','');
		dg1__SwithcUIModf(0);
		dg1__Refresh(1);
	});
	
	// data tab fx slide
	SideBarSlideFx = new Fx.Slide('map-sidebar', {
		mode: 'horizontal',
		duration: 250,
		transition: Fx.Transitions.Cubic.easeOut,
		onComplete: function(){
			olcont_PanZoomBarUpdatePosition();
			if(SideBarMainTabState==0) {
				if(BottomBarMainTabState==1) $('map-bottombar-shadow').setStyle('left',null);	
				$('map-sidebar').set('class', null);	
			} else {
				if(BottomBarMainTabState==1) $('map-bottombar-shadow').setStyle('left','381px');	
				$('map-sidebar').set('class', 'vertshadow');	
			}
						
					
		}
		
	}).hide();

	var SideBarSlideContainer = $('map-sidebar').getParent();
	SideBarSlideContainer.setStyle('position',null);

	$('map-sidebar-slide-toggle-show').setStyle('opacity', '0.75');	
	$('map-sidebar-slide-toggle-show').addEvent('mouseover', function(e){ 
		$('map-sidebar-slide-toggle-show').setStyle('opacity', '0.9');	
	});
	$('map-sidebar-slide-toggle-show').addEvent('mouseout', function(e){ 
		$('map-sidebar-slide-toggle-show').setStyle('opacity', '0.75');	
	});
	
	$('map-sidebar-slide-toggle-show').addEvent('click', function(e){
		e = new Event(e);
		SideBarSlideFx.slideIn();
		e.stop();
		SideBarMainTabState = 1;
		// .. moved to SideBarSlideFx.onComplete
		// if(BottomBarMainTabState==1) $('map-bottombar-shadow').setStyle('left','381px'); 
		
	});		
	$('map-sidebar-slide-toggle-hide').addEvent('click', function(e){
		e = new Event(e);
		SideBarSlideFx.slideOut();
		e.stop();
		SideBarMainTabState = 0;
		if(BottomBarMainTabState==1) $('map-bottombar-shadow').setStyle('left',null);
		
	});
	
	// bottom bar
	$('map-bottombar-slide-toggle-show').setStyle('opacity', '0.75');	
	$('map-bottombar-slide-toggle-show').addEvent('mouseover', function(e){ 
		$('map-bottombar-slide-toggle-show').setStyle('opacity', '0.9');	
	});
	$('map-bottombar-slide-toggle-show').addEvent('mouseout', function(e){ 
		$('map-bottombar-slide-toggle-show').setStyle('opacity', '0.75');	
	});
	
	$('map-bottombar-slide-toggle-show').addEvent('click', function(e){
		e = new Event(e);
		e.stop();
		BottomBarSwitchState(1);
	});		
	$('map-bottombar-slide-toggle-hide').addEvent('click', function(e){
		e = new Event(e);
		e.stop();
		BottomBarSwitchState(0);
	});		
	
	InitialMaps();
	$$(OpenLayers_Control_PanZoomBar_Id).setStyle('left', '30px');
}

var DgRowPopUp=null;
function evhandleDgRowMouseOver(elm,lon,lat,content){
	elm.setStyle('background-color','#FCFCFC');
}
function evhandleDgRowMouseOut(elm){
	elm.setStyle('background-color',null);
	if($defined(DgRowPopUp)){
		map.removePopup(DgRowPopUp);
		DgRowPopUp=null;
	}
}

function olcont_PanZoomBarUpdatePosition(){
	switch (SideBarMainTabState){
		case 0:
			//$(OpenLayers_Control_PanZoomBar_Id).setStyle('top', '30px');	
			$$(OpenLayers_Control_PanZoomBar_Id).setStyle('left', '30px');
			//$$(OpenLayers_Control_ScaleLine_Id).setStyle('left', '100px');						
			break;
		case 1:
			//$$(OpenLayers_Control_PanZoomBar_Id).setStyle('top', '30px');	
			$$(OpenLayers_Control_PanZoomBar_Id).setStyle('left', '400px');
			//$$(OpenLayers_Control_ScaleLine_Id).setStyle('left', '400px');
			break;
	}
}

function InitialMaps()
{
	var textent = ol.proj.transformExtent([95, -11, 150, 6], 'EPSG:4326', 'EPSG:3857');
	var maxExtent = ol.proj.transform([118.95996093749999, -4.828259746866976], "EPSG:4326", "EPSG:3857");
	var googleLayer = new olgm.layer.Google();	
	var osmLayer = new ol.layer.Tile({
            source: new ol.source.OSM()
         });
	
	var map = new ol.Map({
		target: 'map',
        layers: [
			new ol.layer.Group({
				'title': 'OSM maps',
				layers: [osmLayer]
			})
		],
        controls: ol.control.defaults({
          attributionOptions: /** @type {olx.control.AttributionOptions} */ ({
            collapsible: false
          })
        }),
        view: new ol.View({
			center : maxExtent,	
			zoom : 5,
			minZoom : 5,
			maxZoom : 17,
			extent:textent
        })
      });
	
	
	var layerSwitcher = new ol.control.LayerSwitcher({
        tipLabel: 'Legend' // Optional label for button
    });
	//map.addControl(layerSwitcher);

	var vector = new ol.layer.Vector({
	  source: new ol.source.Vector()
	});
	map.addLayer(vector);
	
}






	

