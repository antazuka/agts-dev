<?php

namespace App\Http\Controllers;
use File;
use Session;
use App\Classes\HelperCustom;
use App\Models\Module;
use App\Models\SubModule;
use App\Models\UserSys;
use Ramsey\Uuid\Uuid;
use DateTime;

use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\Paginator;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Contracts\Routing\ResponseFactory;

class Result
{
	public $success;
	public $errormessage;
	public $url;
	public $data;
	public $rowsperpage = 0; 	
	public $pagenum=0;
	public $maxrow=0;
	public $norow=false;
	public $selectopt;

}

class InsertUpdate
{
	public $success;
	public $errormessage;
	public $data;
	public $recnum;
	
	public $ud_module_uid;		
	public $ud_module_id;		
	public $ud_module_name;
	public $ud_isactive;
	public $ud_description;
	public $ud_sort_order;

	public $ud_create_by;
	public $ud_create_byfn;
	public $ud_create_at;
	public $ud_update_by;
	public $ud_update_byfn;
	public $ud_update_at;
	
}

class InsertUpdateChld
{
	public $success;
	public $errormessage;
	public $data;
	public $recnum;
	
	public $ud_submodule_uid;		
	public $ud_module_uid;
	public $ud_submodule_id;
	public $ud_submodule_name;
	public $ud_submodule_parent_uid;
	public $ud_submodule_class;
	public $ud_submodule_namespace;
	public $ud_submodule_path;
	public $ud_isactive;
	public $ud_description;
	public $ud_sort_order;

	public $ud_create_by;
	public $ud_create_byfn;
	public $ud_create_at;
	public $ud_update_by;
	public $ud_update_byfn;
	public $ud_update_at;
	
}

class DeleteResult
{
	public $success;
	public $errormessage;
	public $deletedid;
}	

class RegisterModuleController extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;
	
	public function index()
	{
		
		return view('module.registermodule.index',[
			'js' => 'mootools',
        ]);
		//return view('module.registermodule.index');
	}
	
	public function show()
	{
		$Res = new Result();
		$RowsPerPage = Input::get('rowsperpage');
		$PageNum = Input::get('pagenum');
		$CurentNav = Input::get('curentnav');
		$search = Input::get('search');
		$MaxRow = 0; 
		$offset = ($PageNum-1)*$RowsPerPage;
		
		// prepare file base variable
		$sbRowBody = File::get(storage_path('common/rowsbody.txt'));
		$sbRowItems ='';
		$getReplaceRowBody = '';
		$RowItemTemplate = view('module.registermodule.row');
		$RowEmpty = view('module.registermodule.rowEmpty');
				
		$modules = Module::getByPage($RowsPerPage,$offset,$search);
		
		//dd($modules->selectrow);
		foreach($modules->maxrow as $key=>$value)
		{
			$MaxRow = $value['maxrow'];
		}
		//dd($MaxRow);
		if(!empty($modules->selectrow))
		{
			//$MaxRow = Module::count();

			foreach($modules->selectrow as $module)
			{
				
				$isactive;
				if($module['isactive'] == 'Aktif'){$isactive = 1;}else{$isactive = 0;} 
				
				$sbRowItems = $sbRowItems.$RowItemTemplate;
				$sbRowItems = str_replace('#ud_module_uid#',$module['module_uid'],$sbRowItems);
				$sbRowItems = str_replace('#ud_module_id#',$module['module_id'],$sbRowItems);
				$sbRowItems = str_replace('#ud_module_name#',$module['module_name'],$sbRowItems);
				$sbRowItems = str_replace('#ud_sort_order#',$module['sort_order'],$sbRowItems);
				$sbRowItems = str_replace('#ud_isactive#',$isactive,$sbRowItems);
				$sbRowItems = str_replace('#ud_description#',$module['description'],$sbRowItems);
				$sbRowItems = str_replace('#ud_create_by#',$module['create_by'],$sbRowItems);
				$sbRowItems = str_replace('#ud_create_byfn#',$module['create_byfn'],$sbRowItems);
				$sbRowItems = str_replace('#ud_create_at#',$module['create_at'],$sbRowItems);
				$sbRowItems = str_replace('#ud_update_by#',$module['update_by'],$sbRowItems);
				$sbRowItems = str_replace('#ud_update_byfn#',$module['update_byfn'],$sbRowItems);
				$sbRowItems = str_replace('#ud_update_at#',$module['update_at'],$sbRowItems);
				
				//if($module['isactive'] == 1){$isactive = 'Aktif';}else{$isactive = 'Tidak Aktif';} 
				
				
				$sbRowItems = str_replace('#recnum#',$module['recnum'],$sbRowItems);
				$sbRowItems = str_replace('#module_uid#',$module['module_uid'],$sbRowItems);
				$sbRowItems = str_replace('#module_id#',$module['module_id'],$sbRowItems);
				$sbRowItems = str_replace('#module_name#',$module['module_name'],$sbRowItems);
				$sbRowItems = str_replace('#sort_order#',$module['sort_order'],$sbRowItems);
				$sbRowItems = str_replace('#isactive#',$module['isactive'],$sbRowItems);
				$sbRowItems = str_replace('#description#',$module['description'],$sbRowItems);
				$sbRowItems = str_replace('#create_by#',$module['create_by'],$sbRowItems);
				$sbRowItems = str_replace('#create_byfn#',$module['create_byfn'],$sbRowItems);
				$sbRowItems = str_replace('#create_at#',$module['create_at'],$sbRowItems);
				$sbRowItems = str_replace('#update_by#',$module['update_by'],$sbRowItems);
				$sbRowItems = str_replace('#update_byfn#',$module['update_byfn'],$sbRowItems);
				$sbRowItems = str_replace('#update_at#',$module['update_at'],$sbRowItems);
				
			}
			
			$getReplaceRowBody = str_replace('#ROWS#',$sbRowItems,$sbRowBody);
			// generate temp file
			//File::delete(storage_path('temp/testfile.xml'));
			//$tmpfilename = File::put(storage_path('temp/testfile.xml'),$getReplaceRowBody);
			
			$Res->success = 'true';
			$Res->errormessage = '';
			$Res->data = $getReplaceRowBody;					
			$Res->rowsperpage = (int)$RowsPerPage;
			$Res->pagenum = (int)$PageNum;
			$Res->maxrow = $MaxRow;
			$Res->norow = false;
			//$Res->Url = storage_path('temp\testfile.xml');
			
		}else
		{
			$getReplaceRowBody = str_replace('#ROWS#',$RowEmpty,$sbRowBody);
			// generate temp file
			//File::delete(storage_path('temp/testfile.xml'));
			//$tmpfilename = File::put(storage_path('temp/testfile.xml'),$getReplaceRowBody);
			
			$Res->success = 'true';
			$Res->errormessage = '';
			$Res->data = $getReplaceRowBody;
			$Res->rowsperpage = (int)$RowsPerPage;
			$Res->pagenum = (int)$PageNum;
			$Res->maxrow = 0;
			$Res->norow = true;
			//$Res->Url = storage_path('temp\testfile.xml');
			
		}

		return response()->json($Res);
		
	}
	
	public function insert()
	{
		$Res = new InsertUpdate();
		
		$sbRowBody = File::get(storage_path('common/rowsbody.txt'));
		$RowItemTemplate = view('module.registermodule.row');
		$sbRowItems ='';
		$getReplaceRowBody = '';
		
		$module_uid = Input::get('module_uid');
		$module_id = Input::get('module_id');
		$module_name = Input::get('module_name');
		$sort_order = Input::get('sort_order');
		$isactive = Input::get('isactive');
		$description = Input::get('description');
		$create_by = Input::get('create_by');
		$create_byfn = Input::get('create_byfn');
		
		
		$new_uid = Uuid::uuid4();
		$modules = new Module;
		$modules->module_uid = $new_uid;
		$modules->module_id = $module_id;		
		$modules->module_name = $module_name;
		$modules->isactive = $isactive;
		$modules->sort_order = $sort_order;
		$modules->description = $description;
		
		$modules->create_by = $create_by;
		$modules->update_by = $create_by;
		
		$saved = $modules->save();
		
		$create_at = $modules->create_at;
		
		if($saved)
		{

			$usersys = UserSys::where('user_id','=', $create_by)->first();	
			$create_byfn = $usersys->user_name;
			
			$MaxRow = Module::count();
			$isactive_var;
			if($isactive == 1){$isactive_var = 'Aktif';}else{$isactive_var = 'Tidak Aktif';} 
			
			$sbRowItems = $sbRowItems.$RowItemTemplate;	
			$sbRowItems = str_replace('#recnum#',$MaxRow,$sbRowItems);
			$sbRowItems = str_replace('#module_uid#',$new_uid,$sbRowItems);
			$sbRowItems = str_replace('#module_id#',$module_id,$sbRowItems);
			$sbRowItems = str_replace('#module_name#',$module_name,$sbRowItems);
			$sbRowItems = str_replace('#sort_order#',$sort_order,$sbRowItems);
			$sbRowItems = str_replace('#isactive#',$isactive_var,$sbRowItems);
			$sbRowItems = str_replace('#description#',$description,$sbRowItems);
			
			$sbRowItems = str_replace('#create_by#',$create_by,$sbRowItems);
			$sbRowItems = str_replace('#create_byfn#',$create_byfn,$sbRowItems);
			$sbRowItems = str_replace('#create_at#',date_format($create_at,"Y-m-d"),$sbRowItems);
			$sbRowItems = str_replace('#update_by#',$create_by,$sbRowItems);
			$sbRowItems = str_replace('#update_byfn#',$create_byfn,$sbRowItems);
			$sbRowItems = str_replace('#update_at#',date_format($create_at,"Y-m-d"),$sbRowItems);
			
			$getReplaceRowBody = str_replace('#ROWS#',$sbRowItems,$sbRowBody);
			$HelperCustom = new HelperCustom();
			$ConvertCol = $HelperCustom->ConvertXmlColToArray($getReplaceRowBody);
			
			$Res->data = (string)$ConvertCol;				
			$Res->ud_module_uid = $new_uid;		
			$Res->ud_module_id = $module_id;		
			$Res->ud_module_name = $module_name;
			$Res->ud_isactive = $isactive;
			$Res->ud_sort_order = $sort_order;
			$Res->ud_description = $description;
			
			$Res->ud_create_by = $create_by;
			$Res->ud_create_byfn = $create_byfn;
			$Res->ud_create_at = $create_at;
			$Res->ud_update_by = $create_by;
			$Res->ud_update_byfn = $create_byfn;
			$Res->ud_update_at = $create_at;
			
			$Res->success = 'true';
			$Res->errormessege = '';
			$Res->recnum = $MaxRow;
		}else
		{
			$Res->success = 'false';
			$Res->errormessege = 'Error Insert';
		}
		
		return response()->json($Res);
		
	}	
	
	public function update()
	{
		$Res = new InsertUpdate();
		
		$sbRowBody = File::get(storage_path('common/rowsbody.txt'));
		$RowItemTemplate = view('module.registermodule.rowNew');
		$sbRowItems ='';
		$getReplaceRowBody = '';
		
		$module_uid = Input::get('module_uid');
		$module_id = Input::get('module_id');
		$module_name = Input::get('module_name');
		$sort_order = Input::get('sort_order');
		$isactive = Input::get('isactive');
		$description = Input::get('description');
		
		$create_by = Input::get('create_by');
		$create_byfn = Input::get('create_byfn');
		$update_by = Input::get('update_by');
		$update_byfn = Input::get('update_byfn');
		
		$rowidx = Input::get('rowidx');

		$modules = Module::where('module_uid','=', $module_uid)->first();		
		$modules->module_id = $module_id;		
		$modules->module_name = $module_name;
		$modules->isactive = $isactive;
		$modules->sort_order = $sort_order;
		$modules->description = $description;
		$modules->update_by = $update_by;
		
		$saved = $modules->save();
		
		$create_at = $modules->create_at;
		$update_at = $modules->update_at;
		
		if($saved)
		{
			$usersys = UserSys::where('user_id','=', $update_by)->first();	
			$update_byfn = $usersys->user_name;
			
			$isactive_var;
			if($isactive == 1){$isactive_var = 'Aktif';}else{$isactive_var = 'Tidak Aktif';} 
			
			$sbRowItems = $sbRowItems.$RowItemTemplate;	
			$sbRowItems = str_replace('#recnum#',$rowidx,$sbRowItems);
			$sbRowItems = str_replace('#module_uid#',$module_uid,$sbRowItems);
			$sbRowItems = str_replace('#module_id#',$module_id,$sbRowItems);
			$sbRowItems = str_replace('#module_name#',$module_name,$sbRowItems);
			$sbRowItems = str_replace('#sort_order#',$sort_order,$sbRowItems);
			$sbRowItems = str_replace('#isactive#',$isactive_var,$sbRowItems);
			$sbRowItems = str_replace('#description#',$description,$sbRowItems);
			
			$sbRowItems = str_replace('#create_by#',$create_by,$sbRowItems);
			$sbRowItems = str_replace('#create_byfn#',$create_byfn,$sbRowItems);
			$sbRowItems = str_replace('#create_at#',date_format(new DateTime($create_at),"Y-m-d"),$sbRowItems);
			$sbRowItems = str_replace('#update_by#',$update_by,$sbRowItems);
			$sbRowItems = str_replace('#update_byfn#',$update_byfn,$sbRowItems);
			$sbRowItems = str_replace('#update_at#',date_format($update_at,"Y-m-d"),$sbRowItems);
			
			
			$getReplaceRowBody = str_replace('#ROWS#',$sbRowItems,$sbRowBody);
			$HelperCustom = new HelperCustom();
			$ConvertCol = $HelperCustom->ConvertXmlColToArray($getReplaceRowBody);
			
			$Res->data = (string)$ConvertCol;			
			$Res->ud_module_uid = $module_uid;		
			$Res->ud_module_id = $module_id;		
			$Res->ud_module_name = $module_name;
			$Res->ud_isactive = $isactive;
			$Res->ud_sort_order = $sort_order;
			$Res->ud_description = $description;
			
			$Res->ud_create_by = $create_by;
			$Res->ud_create_byfn = $create_byfn;
			$Res->ud_create_at = $create_at;
			$Res->ud_update_by = $create_by;
			$Res->ud_update_byfn = $create_byfn;
			$Res->ud_update_at = $create_at;
			
			$Res->success = 'true';
			$Res->errormessege = '';
			$Res->recnum=$rowidx;
			
		}else
		{
			$Res->success = 'false';
			$Res->errormessege = 'Error Insert';
		}
				
		return response()->json($Res);
		
	}	
	
	public function delete()
	{
		$Res = new DeleteResult();
		$module_uid = explode(',', Input::get('module_uid'));
		$tags = array();
		foreach ($module_uid as $key=>$value){
			$tags[$key]=$value;
		}
		$modules = Module::whereIn('module_uid', $tags)->delete();	
		if($modules)
		{
			$Res->success = 'true';
			$Res->errormessege = '';
			$Res->deletedid = $module_uid;
		}else
		{
			$Res->success = 'false';
			$Res->errormessege = 'Error delete';
			$Res->deletedid = $module_uid;
		}
		return response()->json($Res);
		
	}
	
	public function showChld()
	{
		$Res = new Result();
		$RowsPerPage = Input::get('rowsperpage');
		$PageNum = Input::get('pagenum');
		$CurentNav = Input::get('curentnav');
		$search = Input::get('search');
		$module_uid = Input::get('module_uid');
		$MaxRow = 0; 
		$offset = ($PageNum-1)*$RowsPerPage;
		
		// prepare file base variable
		$sbRowBody = File::get(storage_path('common/rowsbody.txt'));
		$sbRowItems ='';
		$getReplaceRowBody = '';
		$RowItemTemplate = view('module.registermodule.row2');
		$RowEmpty = view('module.registermodule.rowEmpty');
				
		$submodules = SubModule::getByPage($RowsPerPage,$offset,$search,$module_uid);
		foreach($submodules->maxrow as $key=>$value)
		{
			$MaxRow = $value['maxrow'];
		}
		if(!empty($submodules->selectrow))
		{
			foreach($submodules->selectrow as $submodule)
			{
				$isactive;
				if($submodule['isactive'] == 1){$isactive = 'True';}else{$isactive = 'False';} 
				
				$sbRowItems = $sbRowItems.$RowItemTemplate;
				$sbRowItems = str_replace('#ud_submodule_uid#',$submodule['submodule_uid'],$sbRowItems);
				$sbRowItems = str_replace('#ud_submodule_id#',$submodule['submodule_id'],$sbRowItems);
				$sbRowItems = str_replace('#ud_submodule_name#',$submodule['submodule_name'],$sbRowItems);				
				$sbRowItems = str_replace('#ud_submodule_parent_uid#',$submodule['submodule_parent_uid'],$sbRowItems);
				$sbRowItems = str_replace('#ud_submodule_parent_name#',$submodule['submodule_parent_name'],$sbRowItems);
				$sbRowItems = str_replace('#ud_submodule_path#',$submodule['submodule_path'],$sbRowItems);
				$sbRowItems = str_replace('#ud_module_uid#',$submodule['module_uid'],$sbRowItems);
				$sbRowItems = str_replace('#ud_module_name#',$submodule['module_name'],$sbRowItems);
				$sbRowItems = str_replace('#ud_submodule_class#',$submodule['submodule_class'],$sbRowItems);
				$sbRowItems = str_replace('#ud_submodule_namespace#',$submodule['submodule_namespace'],$sbRowItems);
				$sbRowItems = str_replace('#ud_sort_order#',$submodule['sort_order'],$sbRowItems);
				$sbRowItems = str_replace('#ud_isactive#',$isactive,$sbRowItems);
				$sbRowItems = str_replace('#ud_description#',$submodule['description'],$sbRowItems);
				
				$sbRowItems = str_replace('#ud_create_by#',$submodule['create_by'],$sbRowItems);
				$sbRowItems = str_replace('#ud_create_byfn#',$submodule['create_byfn'],$sbRowItems);
				$sbRowItems = str_replace('#ud_create_at#',$submodule['create_at'],$sbRowItems);
				$sbRowItems = str_replace('#ud_update_by#',$submodule['update_by'],$sbRowItems);
				$sbRowItems = str_replace('#ud_update_byfn#',$submodule['update_byfn'],$sbRowItems);
				$sbRowItems = str_replace('#ud_update_at#',$submodule['update_at'],$sbRowItems);
				
				if($submodule['isactive'] == 1){$isactive = 'Aktif';}else{$isactive = 'Tidak Aktif';} 
				
				$sbRowItems = str_replace('#recnum#',$submodule['recnum'],$sbRowItems);
				$sbRowItems = str_replace('#submodule_uid#',$submodule['submodule_uid'],$sbRowItems);
				$sbRowItems = str_replace('#submodule_id#',$submodule['submodule_id'],$sbRowItems);
				$sbRowItems = str_replace('#submodule_name#',$submodule['submodule_name'],$sbRowItems);
				$sbRowItems = str_replace('#submodule_parent_name#',$submodule['submodule_parent_name'],$sbRowItems);
				$sbRowItems = str_replace('#sort_order#',$submodule['sort_order'],$sbRowItems);
				$sbRowItems = str_replace('#isactive#',$isactive,$sbRowItems);
				$sbRowItems = str_replace('#description#',$submodule['description'],$sbRowItems);
				
				$sbRowItems = str_replace('#create_by#',$submodule['create_by'],$sbRowItems);
				$sbRowItems = str_replace('#create_byfn#',$submodule['create_byfn'],$sbRowItems);
				$sbRowItems = str_replace('#create_at#',$submodule['create_at'],$sbRowItems);
				$sbRowItems = str_replace('#update_by#',$submodule['update_by'],$sbRowItems);
				$sbRowItems = str_replace('#update_byfn#',$submodule['update_byfn'],$sbRowItems);
				$sbRowItems = str_replace('#update_at#',$submodule['update_at'],$sbRowItems);
				
			}
			
			$getReplaceRowBody = str_replace('#ROWS#',$sbRowItems,$sbRowBody);
			$Res->success = 'true';
			$Res->errormessage = '';
			$Res->data = $getReplaceRowBody;					
			$Res->rowsperpage = (int)$RowsPerPage;
			$Res->pagenum = (int)$PageNum;
			$Res->maxrow = $MaxRow;
			$Res->norow = false;
			$Res->selectopt = $this->HtmlSelectSubParent($module_uid);
			
		}else
		{
			$getReplaceRowBody = str_replace('#ROWS#',$RowEmpty,$sbRowBody);

			$Res->success = 'true';
			$Res->errormessage = '';
			$Res->data = $getReplaceRowBody;
			$Res->rowsperpage = (int)$RowsPerPage;
			$Res->pagenum = (int)$PageNum;
			$Res->maxrow = 0;
			$Res->norow = true;
			$Res->selectopt = $this->HtmlSelectSubParent($module_uid);
		}

		return response()->json($Res);
		
	}
	
	public function HtmlSelectSubParent($module_uid)
	{
		$Res = "";
		$id = "inp-chd1-submodule_parent_uid";
		$valueNull = "00000000-0000-0000-0000-000000000000";
		$SubModulePar = SubModule::where(function($query)
						{
							$query->whereNull('submodule_parent_uid');
							$query->orWhere('submodule_parent_uid','=','');
							$query->orWhere('submodule_parent_uid','=','00000000-0000-0000-0000-000000000000');
						})->Where('module_uid','=',$module_uid)->get(['submodule_uid', 'submodule_name']);
		
		
		$htmluid = "<select id='".$id."' >";
		$htmluid = $htmluid."<option value='".$valueNull."'></option>";
		if(!empty($SubModulePar))
		{
			$option ="";
			foreach($SubModulePar as $parent)
			{
				$option = $option."<option value='".$parent['submodule_uid']."'>".$parent['submodule_name']."</option>";
			}
			$htmluid = $htmluid.$option."</select>";
		}else{
			$htmluid = $htmluid."</select>";
		}
		$Res = $htmluid;
		return $Res;
	}
	
	public function insertChld()
	{
		$Res = new InsertUpdateChld();
		
		$sbRowBody = File::get(storage_path('common/rowsbody.txt'));
		$RowItemTemplate = view('module.registermodule.row2');
		$sbRowItems ='';
		$getReplaceRowBody = '';
		
		$module_uid = Input::get('module_uid');
		$submodule_uid = Input::get('submodule_uid');
		$submodule_id = Input::get('submodule_id');
		$submodule_name = Input::get('submodule_name');
		$submodule_parent_uid = Input::get('submodule_parent_uid');
		$submodule_parent_name = Input::get('submodule_parent_name');
		$submodule_class = Input::get('submodule_class');
		$submodule_namespace = Input::get('submodule_namespace');
		$submodule_path = Input::get('submodule_path');
		$description = Input::get('description');
		$isactive = Input::get('isactive');
		$sort_order = Input::get('sort_order');
		
		$create_by = Input::get('create_by');
		$create_byfn = Input::get('create_byfn');
		
		$new_uid = Uuid::uuid4();
		$submodules = new SubModule;
		$submodules->submodule_uid = $new_uid;
		$submodules->module_uid = $module_uid;	
		$submodules->submodule_id = $submodule_id;		
		$submodules->submodule_name = $submodule_name;
		$submodules->submodule_parent_uid = $submodule_parent_uid;
		$submodules->submodule_class = $submodule_class;
		$submodules->submodule_namespace = $submodule_namespace;
		$submodules->submodule_path = $submodule_path;
		$submodules->description = $description;
		$submodules->isactive = $isactive;
		$submodules->sort_order = $sort_order;
		
		$submodules->create_by = $create_by;
		$submodules->update_by = $create_by;
		
		$saved = $submodules->save();
		
		$create_at = $submodules->create_at;
		
		if($saved)
		{
			$usersys = UserSys::where('user_id','=', $create_by)->first();	
			$create_byfn = $usersys->user_name;
			
			$MaxRow = SubModule::where('module_uid','=',$module_uid)->count();
			$isactive_var;
			if($isactive == 1){$isactive_var = 'Aktif';}else{$isactive_var = 'Tidak Aktif';} 
			
			$sbRowItems = $sbRowItems.$RowItemTemplate;	
			$sbRowItems = str_replace('#recnum#',$MaxRow,$sbRowItems);
			$sbRowItems = str_replace('#submodule_uid#',$new_uid,$sbRowItems);
			$sbRowItems = str_replace('#submodule_id#',$submodule_id,$sbRowItems);
			$sbRowItems = str_replace('#submodule_name#',$submodule_name,$sbRowItems);
			$sbRowItems = str_replace('#submodule_parent_uid#',$submodule_parent_uid,$sbRowItems);
			$sbRowItems = str_replace('#submodule_parent_name#',$submodule_parent_name,$sbRowItems);
			$sbRowItems = str_replace('#submodule_class#',$submodule_class,$sbRowItems);
			$sbRowItems = str_replace('#submodule_namespace#',$submodule_namespace,$sbRowItems);
			$sbRowItems = str_replace('#submodule_path#',$submodule_path,$sbRowItems);
			$sbRowItems = str_replace('#description#',$description,$sbRowItems);
			$sbRowItems = str_replace('#sort_order#',$sort_order,$sbRowItems);
			$sbRowItems = str_replace('#isactive#',$isactive_var,$sbRowItems);
			
			$sbRowItems = str_replace('#create_by#',$create_by,$sbRowItems);
			$sbRowItems = str_replace('#create_byfn#',$create_byfn,$sbRowItems);
			$sbRowItems = str_replace('#create_at#',date_format($create_at,"Y-m-d"),$sbRowItems);
			$sbRowItems = str_replace('#update_by#',$create_by,$sbRowItems);
			$sbRowItems = str_replace('#update_byfn#',$create_byfn,$sbRowItems);
			$sbRowItems = str_replace('#update_at#',date_format($create_at,"Y-m-d"),$sbRowItems);
			
			$getReplaceRowBody = str_replace('#ROWS#',$sbRowItems,$sbRowBody);
			$HelperCustom = new HelperCustom();
			$ConvertCol = $HelperCustom->ConvertXmlColToArray($getReplaceRowBody);
			
			$Res->data = (string)$ConvertCol;				
			$Res->ud_submodule_uid = $new_uid;		
			$Res->ud_module_uid = $module_uid;		
			$Res->ud_submodule_id = $submodule_id;
			$Res->ud_submodule_name = $submodule_name;
			$Res->ud_submodule_parent_uid = $submodule_parent_uid;
			$Res->ud_submodule_class = $submodule_class;
			$Res->ud_submodule_namespace = $submodule_namespace;
			$Res->ud_submodule_path = $submodule_path;
			$Res->ud_isactive = $isactive;
			$Res->ud_sort_order = $sort_order;
			$Res->ud_description = $description;
			
			$Res->ud_create_by = $create_by;
			$Res->ud_create_byfn = $create_byfn;
			$Res->ud_create_at = $create_at;
			$Res->ud_update_by = $create_by;
			$Res->ud_update_byfn = $create_byfn;
			$Res->ud_update_at = $create_at;
			
			$Res->success = 'true';
			$Res->errormessege = '';
			$Res->recnum = $MaxRow;
		}else
		{
			$Res->success = 'false';
			$Res->errormessege = 'Error Insert';
		}
		
		return response()->json($Res);
		
	}	
	
	public function updateChld()
	{
		$Res = new InsertUpdateChld();
		
		$sbRowBody = File::get(storage_path('common/rowsbody.txt'));
		$RowItemTemplate = view('module.registermodule.rowNew2');
		$sbRowItems ='';
		$getReplaceRowBody = '';
		
		$module_uid = Input::get('module_uid');
		$submodule_uid = Input::get('submodule_uid');
		$submodule_id = Input::get('submodule_id');
		$submodule_name = Input::get('submodule_name');
		$submodule_parent_uid = Input::get('submodule_parent_uid');
		$submodule_parent_name = Input::get('submodule_parent_name');
		$submodule_class = Input::get('submodule_class');
		$submodule_namespace = Input::get('submodule_namespace');
		$submodule_path = Input::get('submodule_path');
		$description = Input::get('description');
		$isactive = Input::get('isactive');
		$sort_order = Input::get('sort_order');
		
		$create_by = Input::get('create_by');
		$create_byfn = Input::get('create_byfn');
		$update_by = Input::get('update_by');
		$update_byfn = Input::get('update_byfn');
		
		$rowidx = Input::get('rowidx');

		$submodules = SubModule::where('submodule_uid','=', $submodule_uid)->first();		
		$submodules->submodule_uid = $submodule_uid;		
		$submodules->module_uid = $module_uid;	
		$submodules->submodule_id = $submodule_id;		
		$submodules->submodule_name = $submodule_name;
		$submodules->submodule_parent_uid = $submodule_parent_uid;
		$submodules->submodule_class = $submodule_class;
		$submodules->submodule_namespace = $submodule_namespace;
		$submodules->submodule_path = $submodule_path;
		$submodules->description = $description;
		$submodules->isactive = $isactive;
		$submodules->sort_order = $sort_order;
		
		$submodules->update_by = $update_by;
		
		$saved = $submodules->save();
		
		$create_at = $submodules->create_at;
		$update_at = $submodules->update_at;
		
		if($saved)
		{
			$usersys = UserSys::where('user_id','=', $update_by)->first();	
			$update_byfn = $usersys->user_name;
			
			$isactive_var;
			if($isactive == 1){$isactive_var = 'Aktif';}else{$isactive_var = 'Tidak Aktif';} 
			
			$sbRowItems = $sbRowItems.$RowItemTemplate;	
			$sbRowItems = str_replace('#recnum#',$rowidx,$sbRowItems);
			$sbRowItems = str_replace('#submodule_uid#',$submodule_uid,$sbRowItems);
			$sbRowItems = str_replace('#submodule_id#',$submodule_id,$sbRowItems);
			$sbRowItems = str_replace('#submodule_name#',$submodule_name,$sbRowItems);
			$sbRowItems = str_replace('#submodule_parent_uid#',$submodule_parent_uid,$sbRowItems);
			$sbRowItems = str_replace('#submodule_parent_name#',$submodule_parent_name,$sbRowItems);
			$sbRowItems = str_replace('#submodule_class#',$submodule_class,$sbRowItems);
			$sbRowItems = str_replace('#submodule_namespace#',$submodule_namespace,$sbRowItems);
			$sbRowItems = str_replace('#submodule_path#',$submodule_path,$sbRowItems);
			$sbRowItems = str_replace('#description#',$description,$sbRowItems);
			$sbRowItems = str_replace('#sort_order#',$sort_order,$sbRowItems);
			$sbRowItems = str_replace('#isactive#',$isactive_var,$sbRowItems);
			
			$sbRowItems = str_replace('#create_by#',$create_by,$sbRowItems);
			$sbRowItems = str_replace('#create_byfn#',$create_byfn,$sbRowItems);
			$sbRowItems = str_replace('#create_at#',date_format(new DateTime($create_at),"Y-m-d"),$sbRowItems);
			$sbRowItems = str_replace('#update_by#',$update_by,$sbRowItems);
			$sbRowItems = str_replace('#update_byfn#',$update_byfn,$sbRowItems);
			$sbRowItems = str_replace('#update_at#',date_format($update_at,"Y-m-d"),$sbRowItems);
			
			
			$getReplaceRowBody = str_replace('#ROWS#',$sbRowItems,$sbRowBody);
			$HelperCustom = new HelperCustom();
			$ConvertCol = $HelperCustom->ConvertXmlColToArray($getReplaceRowBody);
			
			$Res->data = (string)$ConvertCol;			
			$Res->ud_submodule_uid = $submodule_uid;		
			$Res->ud_module_uid = $module_uid;
			$Res->ud_submodule_id = $submodule_id;
			$Res->ud_submodule_name = $submodule_name;
			$Res->ud_submodule_parent_uid = $submodule_parent_uid;
			$Res->ud_submodule_class = $submodule_class;
			$Res->ud_submodule_namespace = $submodule_namespace;
			$Res->ud_submodule_path = $submodule_path;
			$Res->ud_isactive = $isactive;
			$Res->ud_sort_order = $sort_order;
			$Res->ud_description = $description;
			
			$Res->ud_create_by = $create_by;
			$Res->ud_create_byfn = $create_byfn;
			$Res->ud_create_at = $create_at;
			$Res->ud_update_by = $create_by;
			$Res->ud_update_byfn = $create_byfn;
			$Res->ud_update_at = $create_at;
			
			$Res->success = 'true';
			$Res->errormessege = '';
			$Res->recnum=$rowidx;
			
		}else
		{
			$Res->success = 'false';
			$Res->errormessege = 'Error Insert';
		}
				
		return response()->json($Res);
		
	}	
	
	public function deleteChld()
	{
		$Res = new DeleteResult();
		$module_uid = Input::get('module_uid');
		$submodule_uid = explode(',', Input::get('submodule_uid'));
		$tags = array();
		foreach ($submodule_uid as $key=>$value){
			$tags[$key]=$value;
		}
		$modules = SubModule::whereIn('submodule_uid', $tags)->where('module_uid','=',$module_uid)->delete();	
		if($modules)
		{
			$Res->success = 'true';
			$Res->errormessege = '';
			$Res->deletedid = $submodule_uid;
		}else
		{
			$Res->success = 'false';
			$Res->errormessege = 'Error delete';
			$Res->deletedid = $submodule_uid;
		}
		return response()->json($Res);
		
	}
	
	
	
	
	
}
