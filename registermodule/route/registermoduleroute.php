<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
Route::get('/regmodule', function(){
	$module = Module::all()->first();
	 echo $module->module_id;
	 echo $module->module_name;
	
});
*/

Route::get('/regmodule', 'RegisterModuleController@index');
Route::get('/regmodule_getByPage', ['uses'=>'RegisterModuleController@show', 'as' => 'regmodule_getByPage']);
Route::post('/regmodule_ins', ['uses'=>'RegisterModuleController@insert', 'as' => 'regmodule_ins']);
Route::post('/regmodule_upd', ['uses'=>'RegisterModuleController@update', 'as' => 'regmodule_upd']);
Route::delete('/regmodule_del', ['uses'=>'RegisterModuleController@delete', 'as' => 'regmodule_del']);
Route::get('/regmodule_getByPageChld', ['uses'=>'RegisterModuleController@showChld', 'as' => 'regmodule_getByPageChld']);
Route::post('/regmodule_insChld', ['uses'=>'RegisterModuleController@insertChld', 'as' => 'regmodule_ins']);
Route::post('/regmodule_updChld', ['uses'=>'RegisterModuleController@updateChld', 'as' => 'regmodule_upd']);
Route::delete('/regmodule_delChld', ['uses'=>'RegisterModuleController@deleteChld', 'as' => 'regmodule_del']);