﻿<row id="#submodule_uid#">

<userdata name="submodule_uid"><![CDATA[#ud_submodule_uid#]]></userdata>
<userdata name="submodule_id"><![CDATA[#ud_submodule_id#]]></userdata>
<userdata name="submodule_name"><![CDATA[#ud_submodule_name#]]></userdata>
<userdata name="submodule_parent_uid"><![CDATA[#ud_submodule_parent_uid#]]></userdata>
<userdata name="submodule_parent_name"><![CDATA[#ud_submodule_parent_name#]]></userdata>
<userdata name="module_uid"><![CDATA[#ud_module_uid#]]></userdata>
<userdata name="module_name"><![CDATA[#ud_module_name#]]></userdata>
<userdata name="submodule_class"><![CDATA[#ud_submodule_class#]]></userdata>
<userdata name="submodule_namespace"><![CDATA[#ud_submodule_namespace#]]></userdata>
<userdata name="submodule_path"><![CDATA[#ud_submodule_path#]]></userdata>
<userdata name="isactive"><![CDATA[#ud_isactive#]]></userdata>
<userdata name="description"><![CDATA[#ud_description#]]></userdata>
<userdata name="sort_order"><![CDATA[#ud_sort_order#]]></userdata>
<userdata name="create_by"><![CDATA[#ud_create_by#]]></userdata>
<userdata name="create_byfn"><![CDATA[#ud_create_byfn#]]></userdata>
<userdata name="create_at"><![CDATA[#ud_create_at#]]></userdata>
<userdata name="update_by"><![CDATA[#ud_update_by#]]></userdata>
<userdata name="update_byfn"><![CDATA[#ud_update_byfn#]]></userdata>
<userdata name="update_at"><![CDATA[#ud_update_at#]]></userdata>
<cell>0</cell>
<cell><![CDATA[#recnum#]]></cell>
<cell><![CDATA[#submodule_id#]]></cell>
<cell><![CDATA[#submodule_name#]]></cell>
<cell><![CDATA[#submodule_parent_name#]]></cell>
<cell><![CDATA[#isactive#]]></cell>
<cell><![CDATA[#sort_order#]]></cell>
<cell><![CDATA[#description#]]></cell>
<cell><![CDATA[<a id="dg2-row-#recnum#"></a><div style="font-size:7pt;color:gray;">Posted by #create_byfn#, #create_at# <br>Modified by #update_byfn#, #update_at#</div>]]></cell>
</row>
