<?php

namespace App\Models;
use Eloquent;
use DB;
use PDO;

class Result
{
	public $selectrow;
	public $maxrow;
	
}

class Module extends Eloquent {
	
    protected $table = 'module';
	protected $primaryKey = 'module_uid';
	//protected $fillable = ['module_id', 'module_name','sort_order','isactive','description'];
	public $incrementing = false;

	/* timestamps */
	public $timestamps = true;
	const CREATED_AT = 'create_at';
    const UPDATED_AT = 'update_at';

	public function __construct(){
		parent::__construct();
	}
	
	protected $fieldRules = [
		"module_id"		=> ["", ""],
		"module_name"	=> ["required", ""],
		"sort_order"	=> ["", ""],
		"description"	=> ["", ""],
	];
	
	public static function getByPage(
		$var_RowsPerPage,
		$var_PageNum,
		$var_SearchParam)
	{
        $db = DB::connection()->getPdo();
        $stmt = $db->prepare("call sp__module_getByPage (?,?,?)");
		

        $stmt->bindParam(1, $var_RowsPerPage);
        $stmt->bindParam(2, $var_PageNum);
		$stmt->bindParam(3, $var_SearchParam);
        //$stmt->bindParam(3, $out1);
		//$stmt->bindParam(4, $out2);
        $stmt->execute();
        $search = array();
        //dd($stmt);        
		/*
        do {
            $search = $stmt->fetchAll(PDO::FETCH_ASSOC);
			
        } while ($stmt->nextRowset());
		*/
		$Res = new Result();
		$Res->maxrow = $stmt->fetchAll(PDO::FETCH_ASSOC);	
		$stmt->nextRowset();
		$Res->selectrow=$stmt->fetchAll(PDO::FETCH_ASSOC);
         //dd($search); // to see the result
		
		return $Res;		
		
	}
	
	public static function getByPage_old(
		$var_RowsPerPage,
		$var_PageNum,
		$var_MaxRow ,
		$var_SearchParam) 
	{
		$Res = new Result();
		
		// prepare data
		//$modules = Module::where('module_id','like','%*%')->orWhere('module_name','like','%$search%')->orderBy('module_name')->paginate(15);
		
		$Res->selectrow = DB::select('call sp__tes (?,?)',
				array( $var_RowsPerPage, $var_PageNum));
		$Res->maxrow = count($Res->selectrow);
		//$Res->maxrow = $var_MaxRow;
		return $Res;
	
	}
	
	public static function CallRaw($procName, $parameters = null, $isExecute = false)
	{
		$syntax = '';
		for ($i = 0; $i < count($parameters); $i++) {
			$syntax .= (!empty($syntax) ? ',' : '') . '?';
		}
		$syntax = 'CALL ' . $procName . '(' . $syntax . ');';

		$pdo = DB::connection()->getPdo();
		$pdo->setAttribute(\PDO::ATTR_EMULATE_PREPARES, true);
		$stmt = $pdo->prepare($syntax,[\PDO::ATTR_CURSOR=>\PDO::CURSOR_SCROLL]);
		for ($i = 0; $i < count($parameters); $i++) {
			$stmt->bindValue((1 + $i), $parameters[$i]);
		}
		$exec = $stmt->execute();
		if (!$exec) return $pdo->errorInfo();
		if ($isExecute) return $exec;

		$results = [];
		do {
			try {
				$results[] = $stmt->fetchAll(\PDO::FETCH_OBJ);
			} catch (\Exception $ex) {

			}
		} while ($stmt->nextRowset());


		if (1 === count($results)) return $results[0];
		return $results;
	}
		
}

