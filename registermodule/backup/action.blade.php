


@include('module.registermodule.css')




<script src="{{ asset('js/Mootools/mootools-1.2.4-core.js') }} " type="text/javascript"></script>
<script src="{{ asset('js/Mootools/mootools-1.2.4.4-more.js') }}" type="text/javascript"></script>
<script src="{{asset('js/calendar-eightysix-v1.1.js')}}" type="text/javascript"></script>
<script type="text/javascript">

var dg1;
var dg1_Xml;
var dgbox1_Title = 'Register Module ';
var dgbox1_RowsPerPage = 25;
var dgbox1_PageNum = 1;
var dgbox1_PageNumMax = 1;
var dgbox1_MaxRow = 0;
var dgbox1_NoRow = false;
var dgbox1_UiState; //1=insert; 2=update
var dgbox1_AddedRow = 0;
var dgbox1_AddSender = 0; //1=top; 2=bottom
var dgbox1_CurrentRecNum = -1;
var dgbox1_SearchType = 1; // 1=normal search, 2=advanced
var ce1;
var ce2;

document.title = 'AGTS - '+ dgbox1_Title;
InitialComponent();

function InitialComponent()
{
	$('bt-search-go').addEvent('click', function(e){	
		var tes = $('inp-dgbox1-normal-search-q').get('value');
		alert(tes);
	});
	
	$('bt-dg1-refresh-1').addEvent('click', function(e){	
		dg1__Refresh();
	});
	
	// button add
	$('bt-dg1-add-1').addEvent('click', function(e){	
        $('inp-MODULE_UID').set('value','');
		dg1__SwithcUIModf(1);
	});
	
	
	$('bt-dg1-delete-multi-1').addEvent('click', function(e){	
		alert('delete to do');
	});
	
	//back to grid
	$('bt-dgbox1-editor-cancel-1').addEvent('click', function(e){ 
			dg1__SwithcUIModf(0);
			if(dgbox1_CurrentRecNum == 0){
				if(dgbox1_AddSender==1){
					setTimeout("bodyscroll.toTop()", 255);
				} else if(dgbox1_AddSender==2){
					setTimeout("bodyscroll.toBottom()", 255);
				} 
			} else {
				setTimeout("bodyscroll.start(0,$('dg1-row-'+dgbox1_CurrentRecNum).getPosition().y)", 255);			
			}
	});
	
	
	InitialGrid();

}

function InitialGrid()
{
	
	try {
		dg1 = new dhtmlXGridObject('dgbox1');
		dg1.setImagePath('images/dhtmlxGrid/');
		// #region Codegen : JS#00 Manually
		dg1.setHeader(',No,Module,Description,Date of Record');
		dg1.setInitWidths('20,25,250,*,200,200');
		dg1.setColAlign('right,center,left,left,left,left');
		dg1.setColVAlign("middle,middle,middle,middle,middle,middle");
		dg1.setColTypes('ch,ro,ro,ro,ro,ro');
		dg1.setColSorting('int,int,str,str,str,na');
		dg1.enableResizing('false,true,true,true,true,true');
		dg1.enableTooltips('false,false,false,false,false,false');
		// #endregion Codegen : JS#00 Manually
		dg1.enableAutoHeight(true);
		dg1.enableMultiline(true);
		dg1.enableColSpan(true);
		dg1.setSkin('clear'); 
		dg1.attachEvent('onRowSelect', function doOnRowSelected(id,ind){
			if(id!='*' && ind!=0 && ind!=4){
				if(!$chk($('inp-MODULE_UID'))) return; // Codegen JS#00 Manually
				$('inp-MODULE_UID').set('value',id);
				$('inp-MODULE_ID').set('value',dg1.getUserData(id,'MODULE_ID'));
				$('inp-MODULE_NAME').set('value',dg1.getUserData(id,'MODULE_NAME'));
				$('inp-DESCRIPTION').set('value',dg1.getUserData(id,'DESCRIPTION'));
				// #endregion Codegen : JS#01
				$('inp-POST_BY').set('value',dg1.getUserData(id,'POST_BY'));
				$('inp-POST_BYFN').set('text',dg1.getUserData(id,'POST_BYFN'));
				$('inp-POST_DATE').set('text',dg1.getUserData(id,'POST_DATE'));
				$('inp-MODF_BY').set('value',dg1.getUserData(id,'MODF_BY'));
				$('inp-MODF_BYFN').set('text',dg1.getUserData(id,'MODF_BYFN'));
				$('inp-MODF_DATE').set('text',dg1.getUserData(id,'MODF_DATE'));
				
				
				dg1__SwithcUIModf(2); 
			}
		});
		dg1.attachEvent('onCheckbox', function doOnCheck(rowId, cellInd, state) {                
			if(dgbox1_NoRow==true) return false;
			 else return true;
		});
		dg1.attachEvent("onRowAdded", function(rId){
			 
		}); 
		dg1.init();   
	}
	finally {
		dg1__Refresh();
	}
	
}

function dg1__Refresh()
{
	var myRequest = new Request({
		url: "{{route('regmodule_getByPage')}}",
		data : "&_token="+$('_token').get('value'),
		method: 'get',
		onRequest: function(){
			if($('hde-loading-info').getStyle('display')=='none')
				$('hde-loading-info').setStyle('display', 'block'); 
		},
		onSuccess: function(Resp){
			var response = JSON.parse(Resp);
			if(response.Success)
			{
				
				dg1.clearAll();
				dg1.parse(response.Data, function(grid_obj,count){
	                dgbox1_AddedRow = 0;
					//delete loaded xml file after successfull loads
					
			     });
				 
			}
			$('inp-dgbox1-normal-search-q').set('value',response.module_name);
		},
		onFailure: function(){
			alert('text', 'Sorry, your request failed :(');
		}
	});
	myRequest.send();
	if($('hde-loading-info').getStyle('display')=='block')
		$('hde-loading-info').setStyle('display', 'none'); 
}


function dg1__SaveUpdte()
{
	
	
}

function dg1__Delete()
{
	
}


function dg1__SwithcUIModf(UIElmnt){
	switch (UIElmnt){
		case 0:
			//show gridbox, hide editor
			// ######## gridbox UI
			$('dgbox1-grid-modf-top').setStyle('display', 'block');	
			$('dgbox1-grid-main').setStyle('display', 'block');
			$('dgbox1-grid-modf-bottom').setStyle('display', 'block');
			// ######## editor UI
			$('dgbox1-editor-head').setStyle('display', 'none');		
			$('dgbox1-editor-main').setStyle('display', 'none');	
			$('dgbox1-editor-modf-bottom').setStyle('display', 'none');
			// ######## editor UI field
			
			break;
		case 1:
        	//hide gridbox, show editor add
			// ######## gridbox UI
			$('dgbox1-grid-modf-top').setStyle('display', 'none');	
			$('dgbox1-grid-main').setStyle('display', 'none');
			$('dgbox1-grid-modf-bottom').setStyle('display', 'none');
			// ######## editor UI
			$('rgn-dgbox1-editor-title').set('text', 'New '+dgbox1_Title);
			$('dgbox1-editor-head').setStyle('display', 'block');	
			$('dgbox1-editor-modf-top-1').setStyle('visibility', 'hidden');
			$('dgbox1-editor-modf-top-2').setStyle('visibility', 'hidden');			
			$('dgbox1-editor-main').setStyle('display', 'block');	
			$('dgbox1-editor-modf-bottom').setStyle('display', 'block');	
			// ######## editor UI field, add

			dgbox1_CurrentRecNum = 0;
			// #region Codegen : JS#04
			$('inp-MODULE_UID').set('value','');
			$('inp-DESCRIPTION').set('value','');
			// #endregion Codegen : JS#04
			$('inp-POST_BYFN').set('text','');
			$('inp-POST_DATE').set('text','');
			$('inp-MODF_BYFN').set('text','');
			$('inp-MODF_DATE').set('text','');
			setTimeout("bodyscroll.toTop()", 255);
			break;
		case 2:
			//hide gridbox, show editor edit
			// ######## gridbox UI
			$('dgbox1-grid-modf-top').setStyle('display', 'none');	
			$('dgbox1-grid-main').setStyle('display', 'none');
			$('dgbox1-grid-modf-bottom').setStyle('display', 'none');
			// ######## editor UI
			$('rgn-dgbox1-editor-title').set('text', 'Edit '+dgbox1_Title);
			$('dgbox1-editor-head').setStyle('display', 'block');	
		
			$('dgbox1-editor-modf-top-1').setStyle('visibility', 'visible');
			$('dgbox1-editor-modf-top-2').setStyle('visibility', 'visible');	
			$('dgbox1-editor-main').setStyle('display', 'block');	
			//$('dgbox1-editor-modf-bottom').setStyle('display', 'block');			
			// ######## editor UI field, edit
			
			
			//$('inp-DATE_HOLIDAY').focus(); // Codegen JS#00 Manually
			dgbox1_CurrentRecNum = dg1.cellById($('inp-MODULE_UID').get('value'), 1).getValue();  // Codegen JS#00 Manually			
			setTimeout("bodyscroll.toTop()", 255);
			break;
	}
    dgbox1_UiState = UIElmnt;
} 	
	
</script>
	
