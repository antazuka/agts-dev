<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
Route::get('/regmodule', function(){
	$module = Module::all()->first();
	 echo $module->module_id;
	 echo $module->module_name;
	
});
*/

Route::get('/comp', 'CompController@index');
Route::get('/comp_getByPage', ['uses'=>'CompController@show', 'as' => 'comp_getByPage']);
Route::post('/comp_ins', ['uses'=>'CompController@insert', 'as' => 'comp_ins']);
Route::post('/comp_upd', ['uses'=>'CompController@update', 'as' => 'comp_upd']);
Route::delete('/comp_del', ['uses'=>'CompController@delete', 'as' => 'comp_del']);
