<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
Route::get('/regmodule', function(){
	$module = Module::all()->first();
	 echo $module->module_id;
	 echo $module->module_name;
	
});
*/

Route::get('/roles', 'RolesController@index');
Route::get('/roles_getByPage', ['uses'=>'RolesController@show', 'as' => 'roles_getByPage']);
Route::post('/roles_ins', ['uses'=>'RolesController@insert', 'as' => 'roles_ins']);
Route::post('/roles_upd', ['uses'=>'RolesController@update', 'as' => 'roles_upd']);
Route::delete('/roles_del', ['uses'=>'RolesController@delete', 'as' => 'roles_del']);
Route::get('/roles_indexSub', ['uses'=>'RolesController@indexSub', 'as' => 'roles_indexSub']);
Route::get('/roles_getByPageMod', ['uses'=>'RolesController@showMod', 'as' => 'roles_getByPageMod']);
Route::post('/roles_AjaxInsChld', ['uses'=>'RolesController@insertChld', 'as' => 'roles_AjaxInsChld']);
Route::delete('/roles_AjaxDelChld', ['uses'=>'RolesController@deleteChld', 'as' => 'roles_AjaxDelChld']);
Route::post('/roles_AjaxUpdChld', ['uses'=>'RolesController@updateChld', 'as' => 'roles_AjaxUpdChld']);