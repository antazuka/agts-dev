<?php

namespace App\Models;
use Eloquent;
use DB;
use PDO;

class ResultUserSys
{
	public $selectrow;
	public $maxrow;
	
}

class MapDashboard extends Eloquent {
	
    protected $table = 'user_sys';
	protected $primaryKey = 'user_id';
	public $incrementing = false;

	/* timestamps */
	public $timestamps = true;
	const CREATED_AT = 'create_at';
    const UPDATED_AT = 'update_at';

	public function __construct(){
		parent::__construct();
	}
	
		
	public static function getByPage(
		$var_RowsPerPage,
		$var_PageNum,
		$var_SearchParam)
	{
        $db = DB::connection()->getPdo();
        $stmt = $db->prepare("call sp__usersys_getByPage (?,?,?)");
        $stmt->bindParam(1, $var_RowsPerPage);
        $stmt->bindParam(2, $var_PageNum);
		$stmt->bindParam(3, $var_SearchParam);
        $stmt->execute();
        $search = array();
		$Res = new ResultUserSys();
		$Res->maxrow = $stmt->fetchAll(PDO::FETCH_ASSOC);	
		$stmt->nextRowset();
		$Res->selectrow=$stmt->fetchAll(PDO::FETCH_ASSOC);
         //dd($search); // to see the result
		
		return $Res;		
		
	}
	
	
	
	
	
	
		
}

